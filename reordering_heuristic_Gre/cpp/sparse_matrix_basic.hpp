#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <utility>
#include <cassert>
#include <cmath>



// ===== Structure of the nnz-count sparse matrix ===== 
// (elements are the number of nnz of a block up to 16*16)
// Format is roughtly CSR


// https://en.cppreference.com/w/cpp/language/types
struct sparse_mat_elem {
	unsigned short int ind_col;	 // Index of the column of the value (=> N < 65536)
	unsigned short int val;  // 16 bits = 2 octets  (need to have [0, 256] if blocks 16*16)
};

struct sparse_mat {
	// Sizes of the sparse matrix
	unsigned short int num_row;
	unsigned short int num_col;

	// Values
	// The "ind_col" of the elements of the second "std::vector"
	//		are assumed to be sorted in increasing order
	std::vector<std::vector<sparse_mat_elem>> elements;
};

// Utilities
std::vector<sparse_mat_elem> create_sparse_vector(std::vector<unsigned short int> vindcol);

void print_sparse_vector(std::vector<sparse_mat_elem> v);
void print_sparse_mat(struct sparse_mat mat);


struct sparse_mat custom_parse_mtx_file(std::string filename);


// =============================================================================

// ===== Double variant for sparse matrix =====

// Sparse matrix structure storing the real values (and not only a count of nnz for a block)
struct sparse_mat_elem_double {
	unsigned short int ind_col;	 // Index of the column of the value (=> N < 65536)
	double val;
};

struct sparse_mat_double {
	// Sizes of the sparse matrix
	unsigned short int num_row;
	unsigned short int num_col;

	// Values
	// The "ind_col" of the second "std::vector" are assumed to be sorted in increasing order
	std::vector<std::vector<sparse_mat_elem_double>> elements;
};

void print_sparse_vector_double(std::vector<sparse_mat_elem_double> v);
void print_sparse_mat_double(struct sparse_mat_double mat);

void print_ascii_nnz_vect_double(std::vector<sparse_mat_elem_double> v, unsigned short int num_col);
void print_ascii_nnz_mat_double(struct sparse_mat_double mat);

struct sparse_mat_double custom_parse_mtx_file_double(std::string filename);

void save_sparse_mat_double(struct sparse_mat_double mat, std::string filename);


// =============================================================================

// ===== Permutation utilities =====

// Invert a permutation
std::vector<unsigned short int> invert_permutation(std::vector<unsigned short int> perm);

std::vector<unsigned short int> random_permut(unsigned short int size);

void print_permut(std::vector<unsigned short int> v);
