
#include "sparse_matrix_basic.hpp"


// Operations for affinity computation (must be optimized!)
std::vector<sparse_mat_elem> combine_add_vector_rows(
		std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2);

std::vector<sparse_mat_elem> combine_min_vector_rows(
		std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2);


// For affinity V2 (density of nnz in the activated columns)
int card_nnz_combined(std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2);
int card_nnz(std::vector<sparse_mat_elem> v1);


// Needed to switch from columns comparison to rows comparisons
struct sparse_mat transpose_sparse_mat(struct sparse_mat mat);

