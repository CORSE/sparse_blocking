
#include "sparse_matrix_basic.hpp"


// ===== Utilities functions for the nnz-count sparse matrix ===== 

// Custom function - build a sparse vector (with only 0 and 1 values)
//	 given a list of elements for their column position
std::vector<sparse_mat_elem> create_sparse_vector(std::vector<unsigned short int> vindcol) {
	std::vector<sparse_mat_elem> v_res;

	for (unsigned short int indcol : vindcol) {
		struct sparse_mat_elem elem = { indcol, 1};
		v_res.push_back(elem);
	}

	return v_res;
}

// Pretty printer for sparse vector
void print_sparse_vector(std::vector<sparse_mat_elem> v) {
	std::cout << "[ ";

	for (struct sparse_mat_elem elem : v) {
		std::cout << "(" << elem.ind_col << "," << elem.val << ") ";
	}
	std::cout << "]\n";
	return;
}

// Pretty printer for sparse_mat
void print_sparse_mat(struct sparse_mat mat) {
	for (std::vector<sparse_mat_elem> vrow : mat.elements) {
		print_sparse_vector(vrow);
	}
	std::cout << "\n";
}


// MTX (Matrix Market) file parser - inspired from
// https://stackoverflow.com/questions/57075834/how-to-convert-matrix-market-file-to-matrix-in-c
struct sparse_mat custom_parse_mtx_file(std::string filename) {
	// https://en.cppreference.com/w/cpp/io/basic_ifstream/basic_ifstream
	std::ifstream file(filename, std::ios::in);  // std::ios::binary ???
	int num_row, num_col, num_lines;

	//std::cout << "ping - starting parsing\n";

	// First line - check if this is a general matrix
	std::string first_line;
	getline(file, first_line);
	// Note: first_line got all the first line, minus the end

	bool is_symmetric = false;
	auto suffix_symmetric = std::string_view("symmetric");

	if (first_line.ends_with(suffix_symmetric)) {
		is_symmetric = true;
	} else {
		auto suffix_general = std::string_view("general");
		if (not (first_line.ends_with(suffix_general))) {
			std::cout << "ERROR: Non general matrix are not managed by custom_parse_mtx_file!\n";
				std::cout << first_line << "\n\n";
			assert(false);
		}
	}

	// Ignore comments headers
	while (file.peek() == '%') file.ignore(2048, '\n');

	// Read number of rows and columns
	file >> num_row >> num_col >> num_lines;

	// Note: We create the dense matrix before getting the (sparse) matrix of non-zero
	// (because elements of the MTX format might not be in the right order)

	// Note: we throw away the actual values of the matrix

	// To check that the indexes fits a "unsigned short int"
	assert(num_row < 65536);
	assert(num_col < 65536);


	// Create 2D array and fill with zeros
	unsigned short int* matrix = new unsigned short int[num_row * num_col];
	std::fill(matrix, matrix + num_row *num_col, (unsigned short int) 0);

	// fill the matrix with data
	for (int l = 0; l < num_lines; l++) {
	    double data;
	    int row, col;
	    file >> row >> col >> data;

	    if (data!=0.0) {
	    	matrix[(row -1) + (col -1) * num_row] = (unsigned short int) 1;
	    	if (is_symmetric) {
	    		matrix[(col -1) + (row -1) * num_row] = (unsigned short int) 1;
	    	}
	    }
	}
	file.close();

	//std::cout << "ping - dense mat done\n";

	// Convert this dense 2D array into a sparse matrix
	std::vector<std::vector<sparse_mat_elem>> vvelem;
	for (int i=0; i<num_row; i++) {
		std::vector<sparse_mat_elem> velem_row;

		for (int j=0; j<num_col; j++) {
			if (matrix[i + j * num_row] != 0) {
				struct sparse_mat_elem nelem = { (short unsigned int) j, 1 };
				velem_row.push_back(nelem);
			}
		}
		// Sorting velem_row : not needed (because of dense)

		// Don't add the row if it is empty
		//	=> Info of line location not stored
		//if (not velem_row.empty()) {
		vvelem.push_back(velem_row);
		//}
	}

	//std::cout << "ping - vvelem done\n";

	// Wrap up everything
	struct sparse_mat retmat { (short unsigned int) num_row, (short unsigned int) num_col, vvelem};
	return retmat;
}


// =============================================================================

// ===== Double variant for sparse matrix =====


// Pretty printer for sparse vector of double
void print_sparse_vector_double(std::vector<sparse_mat_elem_double> v) {
	std::cout << "[ ";

	for (struct sparse_mat_elem_double elem : v) {
		std::cout << "(" << elem.ind_col << "," << elem.val << ") ";
	}
	std::cout << "]\n";
	return;
}

// Pretty printer for sparse_mat_double
void print_sparse_mat_double(struct sparse_mat_double mat) {
	for (std::vector<sparse_mat_elem_double> vrow : mat.elements) {
		print_sparse_vector_double(vrow);
	}
	std::cout << "\n";
}



// Pretty-printing of the nnz, for terminal (and small matrices) - vector
void print_ascii_nnz_vect_double(std::vector<sparse_mat_elem_double> v,
		unsigned short int num_col) {
	std::cout << "[";
	unsigned short int j=0;
	for (sparse_mat_elem_double e : v) {
		for (unsigned short int k=j; k<e.ind_col; k++)
			std::cout << ".";
		std::cout << "0";
		j = e.ind_col+1;
	}
	for (unsigned short int k=j; k<num_col; k++)
		std::cout << ".";

	std::cout << "]\n";
	return;
}

// Pretty-printing of the nnz, for terminal (and small matrices) - full matrix
void print_ascii_nnz_mat_double(struct sparse_mat_double mat) {
	for (std::vector<sparse_mat_elem_double> vrow : mat.elements) {
		print_ascii_nnz_vect_double(vrow, mat.num_col);
	}
	std::cout << "\n";
}


// Utility
// MTX (Matrix Market) file parser - inspired from
// https://stackoverflow.com/questions/57075834/how-to-convert-matrix-market-file-to-matrix-in-c
//   with additinnal "symmetric" support.
struct sparse_mat_double custom_parse_mtx_file_double(std::string filename) {
	// https://en.cppreference.com/w/cpp/io/basic_ifstream/basic_ifstream
	std::ifstream file(filename, std::ios::in);  // std::ios::binary ???
	int num_row, num_col, num_lines;

	//std::cout << "ping - starting parsing\n";

	// First line - check if this is a general matrix
	std::string first_line;
	getline(file, first_line);
	// Note: first_line got all the first line, minus the ending "\n"

	bool is_symmetric = false;
	auto suffix_symmetric = std::string_view("symmetric");
	
	if (first_line.ends_with(suffix_symmetric)) {
		is_symmetric = true;
	} else {
		auto suffix_general = std::string_view("general");
		if (not (first_line.ends_with(suffix_general))) {
			std::cout << "ERROR: Non general matrix are not managed by custom_parse_mtx_file_double!\n";
			std::cout << first_line << "\n\n";
			assert(false);
		}		
	}


	// Ignore comments headers
	while (file.peek() == '%') file.ignore(2048, '\n');

	// Read number of rows and columns
	file >> num_row >> num_col >> num_lines;

	// Note: We create the dense matrix before getting the (sparse) matrix of non-zero
	// (because elements of the MTX format might not be in the right order)

	// DEBUG
	//std::cout << "num_row = " << num_row << " | num_col = " << num_col << "\n";


	// To check that the indexes fits a "unsigned short int"
	assert(num_row < 65536);
	assert(num_col < 65536);


	// Create 2D array and fill with zeros
	double* matrix = new double[num_row * num_col];
	std::fill(matrix, matrix + num_row *num_col, (unsigned short int) 0);

	// fill the matrix with data
	for (int l = 0; l < num_lines; l++) {
	    double data;
	    int row, col;
	    file >> row >> col >> data;

    	matrix[(row -1) + (col -1) * num_row] = data;
    	if (is_symmetric) {
    		matrix[(col -1) + (row -1) * num_row] = data;
    	}
	}
	file.close();

	//std::cout << "ping - dense mat done\n";

	// Convert this dense 2D array into a sparse matrix
	std::vector<std::vector<sparse_mat_elem_double>> vvelem(num_row);
	for (int i=0; i<num_row; i++) {
		std::vector<sparse_mat_elem_double> velem_row;

		for (int j=0; j<num_col; j++) {
			if (matrix[i + j * num_row] != 0) {
				struct sparse_mat_elem_double nelem =
					{ (short unsigned int) j, matrix[i + j * num_row] };
				velem_row.push_back(nelem);
			}
		}

		// Sort velem_row: not needed
		//std::sort(velem_row.begin(), velem_row.end(), sparse_mat_elem_double_less);

		vvelem[i] = velem_row;
	}

	//std::cout << "ping - vvelem done\n";

	// Wrap up everything
	struct sparse_mat_double retmat { (short unsigned int) num_row,
		(short unsigned int) num_col, vvelem};
	return retmat;
}


// Save a sparse_mat_double in MTX sparse/general format
void save_sparse_mat_double(struct sparse_mat_double mat, std::string filename) {
	std::ofstream outfile;
	outfile.open(filename);

	// Header
	outfile << "%" << "%MatrixMarket matrix coordinate real general\n";

	// Sizes and nnz
	int nnz = 0;
	for (std::vector<sparse_mat_elem_double> vrow : mat.elements)
		nnz += vrow.size();
	outfile << mat.num_row << " " << mat.num_col << " " << nnz << "\n";

	for (unsigned short int i=0; i<mat.num_row; i++) {
		std::vector<sparse_mat_elem_double> vrow = mat.elements[i];

		for (sparse_mat_elem_double elem : vrow) {
			// Numbering starts on 1 (instead of 0)
			outfile << (i+1) << " " << (elem.ind_col+1) << " " << elem.val << "\n";
		}
	}
	outfile.close();

	return;
}



// =============================================================================

// ===== Permutation utilities =====
// Permutation are represented as vectors. Its elements must be 0, 1, ... , (size_perm-1)
//		permuted in a certain order (so, no repetition of elements and no other elements)

// Invert a permutation
std::vector<unsigned short int> invert_permutation(std::vector<unsigned short int> perm) {
	std::vector<unsigned short int> invperm(perm.size());
	for (int i=0; i<perm.size(); i++) {
		invperm[perm[i]] = (unsigned short int) i;
	}
	return invperm;
}

// Create a random permutation of size "size"
std::vector<unsigned short int> random_permut(unsigned short int size) {
	// Do it by insertion
	std::vector<unsigned short int> vres;
	for (unsigned short int i=0; i<size; i++) {
		// We insert "i" in a random position in the current "vres" vector
		double rand_pos = ( ((double) std::rand()) / ((double) RAND_MAX) ) * ((double) i);
		int rand_pos_int = (int) std::floor(rand_pos);

		auto it = vres.begin() + rand_pos_int;
		vres.insert(it, i);
	}

	return vres;
}

// Pretty-printer for permutations
void print_permut(std::vector<unsigned short int> v) {
	std::cout << "[ ";

	for (unsigned short int elem : v) {
		std::cout << elem << ", ";
	}
	std::cout << "]\n";
	return;
}

