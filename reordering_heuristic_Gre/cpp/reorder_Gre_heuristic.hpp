#include <algorithm>
#include <set>
#include <cstdlib>

#include <omp.h>
#include <chrono>

#include "sparse_vector_combi.hpp"


// Hacky way to trigger one of the two affinities, with no loss of perf due to a "ifte",
//		or code duplication
//#define AFFINITY_MIN
#define AFFINITY_DENSITY




// Permutation of sparse matrices according to row/col permutations
struct sparse_mat permute(struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		bool invert);

struct sparse_mat_double permute_double(struct sparse_mat_double matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		bool invert);


// Merge the values of blocks into a single cell of a smaller (sparse) matrix
struct sparse_mat fused(struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j);


// An affinity element (between 2 rows or 2 columns)
struct affinity_elem {
	unsigned short int val;
	unsigned short int ind1;
	unsigned short int ind2;
};

// Return a new permutation for a given axis that maximise affinity between blocks
std::vector<unsigned short int> merge_pairs(
		struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j,
		bool axis);


// Structure for the result of "reordered_algo" (the main function)
struct result_triple {
	sparse_mat_double perm_matrix;
	std::vector<unsigned short int> permut_rows;
	std::vector<unsigned short int> permut_cols;
};

// Return a permuted matrix and its permutations on rows and cols
//		to group nonzeros in blocks of size blocksize_i x blocksize_j.
struct result_triple reordered_algo(sparse_mat_double matrix,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j);


// Cost function to evaluate how good is the repartition of the nnz,
//		in respect to the blocking on blocksize_i * blocksize_j blocks
int cost_func(struct sparse_mat_double matrix,
	unsigned short int blocksize_i,
	unsigned short int blocksize_j,
	int threshold);


// Utilities (for debugging)
void print_affinity_list(std::vector<affinity_elem> vae);
void print_set_tuple_ind(std::set<std::vector<unsigned short int>> couples);
