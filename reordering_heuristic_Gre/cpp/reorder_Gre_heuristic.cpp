// g++ sparse_vector_combi.cpp reorder_Gre_heuristic.cpp -O3 -fopenmp -std=c++20


// C implementation of the heuristic from Valentin, Fabrice (in Python)
#include "reorder_Gre_heuristic.hpp"



// ==================================================
// Permute function

// Comparison operator for sorting a vector of sparse_mat_elem
// https://en.cppreference.com/w/cpp/algorithm/sort
struct {
	bool operator()(sparse_mat_elem a, sparse_mat_elem b) const {
		return a.ind_col < b.ind_col;
	}
} sparse_mat_elem_less;
// Alternative version (performance difference ???)
//bool sparse_mat_elem_less(sparse_mat_elem a, sparse_mat_elem b) {
//	return a.ind_col < b.ind_col;
//}


// Return a new matrix with line swapped according to
//		perm_rows and columns swapped according to permuted_cols.
// permute(matrix: Matrix, permut_rows: VecInt, permut_cols: VecInt) -> Matrix
struct sparse_mat permute(struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		bool invert) {

	if (invert) {
		permut_rows = invert_permutation(permut_rows);
		permut_cols = invert_permutation(permut_cols);
	}


	// Create the returned vect of vect of elements
	std::vector<std::vector<sparse_mat_elem>> nvvelems;
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem> nvelems;
		nvvelems.push_back(nvelems);
	}

	// Fill the returned elements
	// Note: the orders of the ind_col in the rows are not preserved
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem> rowi = matrix.elements[i];
		for (sparse_mat_elem elem : rowi) {
			struct sparse_mat_elem nelem { permut_cols[elem.ind_col], elem.val };
			nvvelems[permut_rows[i]].push_back(nelem);
		}
	}

	// For each rows of nvvelems, we sort the elements according to their ind_col
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem> vrow = nvvelems[i];
		std::sort(vrow.begin(), vrow.end(), sparse_mat_elem_less);
		nvvelems[i] = vrow;
	}

	// Returning the final matrix
	struct sparse_mat ret_mat {matrix.num_row, matrix.num_col, nvvelems};
	return ret_mat;
}


// ==================================================
// Permute function for double sparse mat

// Comparison operator for sorting a vector of sparse_mat_elem_double
struct {
	bool operator()(sparse_mat_elem_double a, sparse_mat_elem_double b) const {
		return a.ind_col < b.ind_col;
	}
} sparse_mat_elem_double_less;

// Version of permute for double matrices
struct sparse_mat_double permute_double(struct sparse_mat_double matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		bool invert) {

	if (invert) {
		permut_rows = invert_permutation(permut_rows);
		permut_cols = invert_permutation(permut_cols);
	}


	// Create the returned vect of vect of elements
	std::vector<std::vector<sparse_mat_elem_double>> nvvelems;
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem_double> nvelems;
		nvvelems.push_back(nvelems);
	}

	// Fill the returned elements
	// Note: the orders of the ind_col in the rows are not preserved
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem_double> rowi = matrix.elements[i];
		for (sparse_mat_elem_double elem : rowi) {
			struct sparse_mat_elem_double nelem { permut_cols[elem.ind_col], elem.val };
			nvvelems[permut_rows[i]].push_back(nelem);
		}
	}

	// For each rows of nvvelems, we sort the elements according to their ind_col
	for (unsigned short int i=0; i<matrix.num_row; i++) {
		std::vector<sparse_mat_elem_double> vrow = nvvelems[i];
		std::sort(vrow.begin(), vrow.end(), sparse_mat_elem_double_less);
		nvvelems[i] = vrow;
	}

	// Returning the final matrix
	struct sparse_mat_double ret_mat {matrix.num_row, matrix.num_col, nvvelems};
	return ret_mat;
}


// ==================================================
// Fused function

//  Return a new matrix of shape N/blocsize_i x M/blocsize_j (matrix's shape is NxM)
//    with elements fused[i,j] = sum(matrix_block[i,j])
//    where matrix_block take care of the given permutations
//    i.e we want the number of nonzeros in the block obtained after permutation.
struct sparse_mat fused(struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j) {

	// Perform the permutation
	struct sparse_mat perm_matrix = permute(matrix, permut_rows, permut_cols, true);

	// DEBUG
	//print_sparse_mat(perm_matrix);

	if ((blocksize_i==1) and (blocksize_j==1)) {
		// Nothing to fused => exit now.
		return perm_matrix;
	}

	unsigned short int n = matrix.num_row;
	unsigned short int m = matrix.num_col;

	// Check the divisibility and get the new sizes
	assert(n % blocksize_i == 0);
	assert(m % blocksize_j == 0);
	unsigned short int n_fused = n / blocksize_i;
	unsigned short int m_fused = m / blocksize_j;

	// Allocate the elements of the new fused matrix
	std::vector<std::vector<sparse_mat_elem>> nvvelems;
	for (unsigned short int i=0; i<n_fused; i++) {
		std::vector<sparse_mat_elem> nvelems;
		nvvelems.push_back(nvelems);
	}

	// Filling
	for (unsigned short int i=0; i<n_fused; i++) {
		for (unsigned short int j=0; j<m_fused; j++) {
			// Origins of the block to be fused
			unsigned short int orig_i = blocksize_i * i;
			unsigned short int orig_j = blocksize_j * j;

			// We sum the values of all this block
			unsigned short int sum_val = 0;

			// Check if we have non-zero indexes in this block and sum their contribution
			for (unsigned short int i_real=orig_i; i_real<orig_i+blocksize_i; i_real++) {
				std::vector<sparse_mat_elem> rowi = perm_matrix.elements[i_real];
				// Note (todo?): possibility of reuse there?

				// Check in rowi if we have interesting elements
				for (sparse_mat_elem elem : rowi) {

					// DEBUG
					//std::cout << "ping - elem " << elem.ind_col << " " << elem.val << "\n";

					if (elem.ind_col < orig_j) {
						continue;
					}
					if (elem.ind_col >= orig_j+blocksize_j) {
						break;	// Elements are sorted
					}

					// DEBUG
					//std::cout << "ping - elem working " << i << " " << j << "\n";

					sum_val += elem.val;
				}
			}

			if (sum_val>0) {
				struct sparse_mat_elem nelem {j, sum_val};
				nvvelems[i].push_back(nelem);
				// Note: should be added in an increasing order
			}
		}
	}

	// Free the (temporary) permuted matrix
	//delete(perm_matrix);

	struct sparse_mat ret_mat {n_fused, m_fused, nvvelems};
	return ret_mat;
}


// ==================================================
// New permutation

// For sorting of couples indexes
struct {
	bool operator()(std::vector<unsigned short int> a, std::vector<unsigned short int> b) const {
		return a[0] < b[0];
	}
} couples_less;


// Return a new permutation on the axis of your choice.
// # Warning : This code is common for row and column and should be used with
//   arguments from one dimension dont mix column with row related data.
std::vector<unsigned short int> new_permutation(
		std::vector<affinity_elem> affinities,
		std::vector<unsigned short int> permutation,
		unsigned short int nblocks,
		unsigned short int blocksize) {

	// Build set of pairs with better affinity
	std::set<unsigned short int> singles;
	for (unsigned short int i=0; i<nblocks; i++) {
		singles.insert(i);
	}
	std::vector<std::vector<unsigned short int>> couples;

	for (affinity_elem aelem : affinities) {
		// If no element remains in single
		if (singles.empty()) {
			break;
		}

		unsigned short int i1 = aelem.ind1;
		unsigned short int i2 = aelem.ind2;

		// Look for i1 and i2 in singles
		bool i1found = singles.contains(i1);
		bool i2found = singles.contains(i2);
		/*
			bool i1found = false;
			bool i2found = false;
			for (unsigned short int e : singles) {
				if (e==i1) {
					i1found = true;
					if (i2found)
						break;
				}
				if (e==i2) {
					i2found = true;
					if (i1found)
						break;
				}
			}
		*/
		if (i1found and i2found) {
			std::vector<unsigned short int> nelem_couples = {i1, i2};
			couples.push_back(nelem_couples);

			// Remove i1 and i2 from singles
			singles.erase(i1);
			singles.erase(i2);
		}
	}
	// Note: we do not need singles anymore here

	std::sort(couples.begin(), couples.end(), couples_less);

	/* DEBUG
	std::cout << "couples =";
	print_set_tuple_ind(couples);
	std::cout << "\n";
	//*/


	// New permutation group the 2 pair's members together
	std::vector<unsigned short int> new_permutation(permutation.size());
	for (int i=0; i<permutation.size(); i++) {
		new_permutation[i] = permutation[i];
	}

	unsigned short int index = 0;
	for (std::vector<unsigned short int> ecouple : couples) {
		unsigned short int j1 = ecouple[0];
		unsigned short int j2 = ecouple[1];

		// Switch j1 and j2 if not in the right order
		if (j1>j2) {
			unsigned short int temp = j1;
			j1 = j2;
			j2 = temp;
		}

		unsigned short int orig_j1 = blocksize * j1;
		unsigned short int orig_j2 = blocksize * j2;

		for (unsigned short int i=orig_j1; i<orig_j1+blocksize; i++) {
			new_permutation[index] = permutation[i];
			index++;
		}
		for (unsigned short int i=orig_j2; i<orig_j2+blocksize; i++) {
			new_permutation[index] = permutation[i];
			index++;
		}
	}

	/* DEBUG
	std::cout << "new_permutation =";
	print_permut(new_permutation);
	std::cout << "\n";
	//*/

	return new_permutation;
}


// ==================================================
// Merge pairs

// Comparison operator for sorting a vector of affinity_elem
// https://en.cppreference.com/w/cpp/algorithm/sort
struct {
	bool operator()(affinity_elem a, affinity_elem b) const {

		return (a.val > b.val) or ((a.val==b.val) and (a.ind1<b.ind1))
			 or ((a.val==b.val) and (a.ind1==b.ind1) and (a.ind2<b.ind2));
	}
} affinity_elem_greater;


// Return a new permutation for a given axis that maximise affinity between blocks.
// # Warning: It assume a matrix with a even number of blocs on the given axis
//	 and will panic with assertion error if not.
std::vector<unsigned short int> merge_pairs(
		struct sparse_mat matrix,
		std::vector<unsigned short int> permut_rows,
		std::vector<unsigned short int> permut_cols,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j,
		bool axis) {		// axis=False => Rows / axis=True => Cols

	unsigned short int size_mat = (axis)?(matrix.num_col):(matrix.num_row);
	unsigned short int blocksize = (axis)?(blocksize_j):(blocksize_i);
	assert(size_mat % blocksize == 0);

	unsigned short int nblocks = size_mat / blocksize;
	assert(nblocks % 2 == 0);

	// Compute fused matrix F
	struct sparse_mat fused_matrix = fused(matrix,
		permut_rows, permut_cols, blocksize_i, blocksize_j);

	// Compute affinity for every j1 =! j2
    // affinity = sum(I=0, I=n/blocsize_i, min(F[I,j1], F[I,j2]))

    /* DEBUG
    std::cout << "[merge_pairs] matrix (" << matrix.num_row
    	<< "," << matrix.num_col << ")\n";
    std::cout << "[merge_pairs] Fused matrix (" << fused_matrix.num_row
    	<< "," << fused_matrix.num_col << ") =\n";
    print_sparse_mat(fused_matrix);
    std::cout << "\n";
    //*/

	// Transpose the matrix if we work on the columns
	if (axis) {
		fused_matrix = transpose_sparse_mat(fused_matrix);
	}


	// Distributed affinity, to avoid sharing along j1 with the omp parallel for
	std::vector<std::vector<affinity_elem> > distr_affinities(nblocks-1);

#pragma omp parallel for
	for (unsigned short int j1=0; j1<nblocks-1; j1++) {
		for (unsigned short int j2=j1+1; j2<nblocks; j2++) {
			std::vector<sparse_mat_elem> v1 = fused_matrix.elements[j1];
			std::vector<sparse_mat_elem> v2 = fused_matrix.elements[j2];

#ifdef AFFINITY_MIN
			// Pointwise minimum (inside of the sum of the computation of affinity)
			std::vector<sparse_mat_elem> v12 = combine_min_vector_rows(v1, v2);
			// Sum over all the elements
			unsigned short int sum_val = 0;
			for (sparse_mat_elem e : v12) {
				sum_val += e.val;
			}
			// Affinity (sum_val = \sum_k min(v1[k], v2[k]) ) is done being computed here
			struct affinity_elem aelem {sum_val, j1, j2};
#endif
#ifdef AFFINITY_DENSITY
			int num_nnz_v1 = card_nnz(v1);
			int num_nnz_v2 = card_nnz(v2);
			int card_nnz_comb = card_nnz_combined(v1, v2);

			unsigned short int aff = 0;
			if (card_nnz_comb!=0) {
				aff = (unsigned short int) round( matrix.num_col * (num_nnz_v1 + num_nnz_v2) / card_nnz_comb);
			}

			struct affinity_elem aelem {aff, j1, j2};
#endif

			// Story affinity away
			distr_affinities[j1].push_back(aelem);
		}
	}

	std::vector<affinity_elem> affinities;
	for (unsigned short int j1=0; j1<nblocks-1; j1++) {
		for (affinity_elem aelem : distr_affinities[j1]) {
			affinities.push_back(aelem);
		}
	}

	std::stable_sort(affinities.begin(), affinities.end(), affinity_elem_greater);

	/* DEBUG
	std::cout << "Affinities =\n";
	print_affinity_list(affinities);
	std::cout << "\n";
	// For the first pass only
	//if (blocksize_i == 1) {
	//	print_sparse_vector(fused_matrix.elements[10]);
	//	print_sparse_vector(fused_matrix.elements[15]);
	//	print_sparse_vector(combine_min_vector_rows(fused_matrix.elements[10], fused_matrix.elements[15]));
	//}
	//*/

	// Wrapping up things
	std::vector<unsigned short int> permut = (axis)?(permut_cols):(permut_rows);
	return new_permutation(affinities, permut, nblocks, blocksize);
}


// ==================================================
// Reordered algo - main function


// Return a permuted matrix and its permutations on rows and cols
//		to group nonzeros in blocks of size blocksize_i x blocksize_j.
// # Warning: Only square blocks are allowed for now if not
//		an assertion error will be raised.
struct result_triple reordered_algo(sparse_mat_double matrix,
		unsigned short int blocksize_i,
		unsigned short int blocksize_j) {

	unsigned short int n = matrix.num_row;
	unsigned short int m = matrix.num_col;

	// Convert matrice of float to matrice of 0 if zero and 1 if nonzero
	std::vector<std::vector<sparse_mat_elem>> nvvelems01(n);
	for (unsigned short int i=0; i<n; i++) {
		for (sparse_mat_elem_double elem : matrix.elements[i]) {
			struct sparse_mat_elem nelem {elem.ind_col, 1};
			nvvelems01[i].push_back(nelem);
		}
	}
	struct sparse_mat matrix01 {n, m, nvvelems01};
	
	//assert(blocksize_i == blocksize_j);		// Not needed anymore
	assert(blocksize_i>0);

	unsigned short int curbloc_i = 1;
	unsigned short int curbloc_j = 1;

	std::vector<unsigned short int> permut_rows(n);
	for (unsigned short int i=0; i<n; i++)
		permut_rows[i] = i;
	std::vector<unsigned short int> permut_cols(m);
	for (unsigned short int j=0; j<m; j++)
		permut_cols[j] = j;
	
	std::cout << "Start algo on matrix of shape " << n << "x" << m << "\n";

	// Clock measurement
	using std::chrono::high_resolution_clock;
	using std::chrono::duration_cast;
	using std::chrono::duration;
	using std::chrono::seconds;
	auto t1 = high_resolution_clock::now();

	// Main loop
	unsigned short int i = 1;
	while ((curbloc_i < blocksize_i) or (curbloc_j < blocksize_j)) {

		// Block time measurement end
		auto start_block_time = high_resolution_clock::now();

		// Main calls (on the rows, then on the cols)
		if (curbloc_i < blocksize_i) {
			permut_rows = merge_pairs(matrix01, permut_rows, permut_cols,
				curbloc_i, curbloc_j, false);
			curbloc_i = curbloc_i * 2;

			/* DEBUG
			std::cout << "Step " << i << " - permut_rows = \n";
			print_permut(permut_rows);
			std::cout << "\n";
			//*/

		}

		if (curbloc_j < blocksize_j) {
			permut_cols = merge_pairs(matrix01, permut_rows, permut_cols,
				curbloc_i, curbloc_j, true);
			curbloc_j = curbloc_j * 2;

			/* DEBUG
			std::cout << "Step " << i << " - permut_cols = \n";
			print_permut(permut_cols);
			std::cout << "\n";
			//*/
		}
		

		// Block time measurement end
		auto end_block_time = high_resolution_clock::now();
		auto time_blocksec = duration_cast<seconds>(end_block_time - start_block_time);
		std::cout << "Step " << i << " done in " << time_blocksec.count() << "s\n";

		i++;
	}

	// Stop clock measurement
	auto t2 = high_resolution_clock::now();
	auto time_sec = duration_cast<seconds>(t2 - t1);
	std::cout << "Algo done in " << time_sec.count() << "s\n";

	/* DEBUG
	std::cout << "Step END - permut_rows = \n";
	print_permut(permut_rows);
	std::cout << "Step END - permut_cols = \n";
	print_permut(permut_cols);
	//*/


	// Building the output
	struct sparse_mat_double perm_matrix = permute_double(matrix, permut_rows, permut_cols, true);
	struct result_triple ret_tuple {perm_matrix, permut_rows, permut_cols};
	return ret_tuple;
}


// ==================================================
// Cost function

int cost_func(struct sparse_mat_double matrix,
	unsigned short int blocksize_i,
	unsigned short int blocksize_j,
	int threshold) {

	unsigned short int n = matrix.num_row;
	unsigned short int m = matrix.num_col;

	assert(n % blocksize_i == 0);
	assert(m % blocksize_j == 0);

	// Convert matrice of float to matrice of 0 if zero and 1 if nonzero
	std::vector<std::vector<sparse_mat_elem>> nvvelems01(n);
	for (unsigned short int i=0; i<n; i++) {
		for (sparse_mat_elem_double elem : matrix.elements[i]) {
			struct sparse_mat_elem nelem {elem.ind_col, 1};
			nvvelems01[i].push_back(nelem);
		}
	}
	struct sparse_mat matrix01 {n, m, nvvelems01};

	//std::cout << "PING DEBUG n=" << n << " - m=" << m << "\n";
	//std::cout << "PING DEBUG bsi=" << blocksize_i << " - bsj=" << blocksize_j << "\n";

	int cost = 0;
	for (unsigned short int i=0; i<n; i+=blocksize_i) {
		for (unsigned short int j=0; j<m; j+=blocksize_j) {
			// DEBUG
			//std::cout << "PING DEBUG " << i << " - " << j << "\n";

			// We sum the values of all this block
			int nnz_block = 0;

			// Check if we have non-zero indexes in this block and sum their contribution
			for (unsigned short int i_real=i; i_real<i+blocksize_i; i_real++) {
				std::vector<sparse_mat_elem> rowi = matrix01.elements[i_real];
				// Note (todo?): possibility of reuse there?

				// Check in rowi if we have interesting elements
				for (sparse_mat_elem elem : rowi) {

					// DEBUG
					//std::cout << "ping - elem " << elem.ind_col << " " << elem.val << "\n";
					if (elem.ind_col < j) {
						continue;
					}
					if (elem.ind_col >= j+blocksize_j) {
						break;	// Elements are sorted
					}

					// DEBUG
					//std::cout << "ping - elem working " << i << " " << j << "\n";

					nnz_block += 1; // Val is always equal to 1
				}
			}
			int new_cost = std::min(nnz_block, threshold);

			/* DEBUG
			std::cout << "Block (" << i << ", " << j << ") -> local_cost = "
				<< new_cost << "\n";
			//*/

			cost += new_cost;
		}
	}

	return cost;
}



// ==================================================
// ==================================================
// ==================================================
// Utilities for debugging

void print_affinity_list(std::vector<affinity_elem> vae) {
	for (affinity_elem ae : vae) {
		std::cout << "(" << ae.val << "," << ae.ind1 << "," << ae.ind2 << ")";
	}
	return;
}

void print_set_tuple_ind(std::set<std::vector<unsigned short int>> couples) {
	std::cout << "{";
	for (std::vector<unsigned short int> cpl : couples) {
		std::cout << "(" << cpl[0] << ", " << cpl[1] << "),";
	}

	std::cout << "}";
	return;
}

