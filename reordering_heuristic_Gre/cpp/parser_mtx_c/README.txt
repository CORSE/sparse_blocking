// Note from Guillaume (2023 Fev 03)

This parser was found here:
 - https://math.nist.gov/MatrixMarket/mmio-c.html
 - https://math.nist.gov/MatrixMarket/mmio-h.html

It is currently NOT used, because of our custom parser (which is currently enough)

It the custom parser encounters matrices other than "general" and "symmetric"
	(an error will be thrown), then either extend the parsers (for sparse_mat and sparse_mat_double),
	or try to use these files.
