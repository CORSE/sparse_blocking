// g++ sparse_vector_combi.cpp -O3 -fopenmp

#include "sparse_vector_combi.hpp"

// For AVX2 intrinsics
// (cf https://www.intel.com/content/www/us/en/docs/intrinsics-guide/index.html#avxnewtechs=AVX2&cats=Arithmetic&ig_expand=107 )
//#include <immintrin.h>  // For later?


// Implem - operations de base en C pour l'algo de sparse matrix
// ==================================================
// https://en.cppreference.com/w/cpp/container/vector


// === Basic operation 1: combining 2 sparse vectors ===

// For the add operator (to avoid control inside, for now)
std::vector<sparse_mat_elem> combine_add_vector_rows(
		std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2) {

	// Algo de base (sans vectorisation/optimisation)
	unsigned int size_v1 = v1.size();
	unsigned int size_v2 = v2.size();

	// Where in the vectors v1/v2 we are
	unsigned int i1 = 0;
	unsigned int i2 = 0;

	// Resulting vector
	// Pre-allocations tested: max(size_v1, size_v2) and max(size_v1, size_v2) + 16
	// 	All of them slow the program down... :/
	// TODO: this was not a preallocation, but a pre-fill
	//		=> Need to reinvestiguate this claim.
	std::vector<sparse_mat_elem> v_res; //(std::max(size_v1, size_v2) + 16);

	while (1) {

		// Termination condition : if one of the vectors is done
		if (i1==size_v1) {
			// Put the remaining elements of v2 in v_res
			for (; i2<size_v2; i2++)
				v_res.push_back(v2[i2]);
			return v_res;
		}
		if (i2==size_v2) {
			// Put the remaining elements of v1 in v_res
			for (; i1<size_v1; i1++)
				v_res.push_back(v1[i1]);
			return v_res;
		}

		// We have the garanty that there are still elements on both sides

		// Get the two fist elements
		struct sparse_mat_elem e1 = v1[i1];
		struct sparse_mat_elem e2 = v2[i2];

		if (e1.ind_col < e2.ind_col) {
			// No conflict - e1 first
			v_res.push_back(e1);
			i1++;
		} else if (e1.ind_col > e2.ind_col) {
			// No conflict - e2 first
			v_res.push_back(e2);
			i2++;
		} else {
			// e1.ind_col = e2.ind_col

			// Confict => we use the combination operator here
			unsigned short int nval = e1.val + e2.val;
			struct sparse_mat_elem e12 = { e1.ind_col, nval };
			v_res.push_back(e12);
			i1++;
			i2++;
		}
	}
}

// For the min operator (to avoid control inside, for now)
std::vector<sparse_mat_elem> combine_min_vector_rows(
		std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2) {

	// Algo de base (sans vectorisation/optimisation)
	unsigned int size_v1 = v1.size();
	unsigned int size_v2 = v2.size();

	// Where in the vectors v1/v2 we are
	unsigned int i1 = 0;
	unsigned int i2 = 0;

	// Resulting vector
	// Pre-allocations tested: max(size_v1, size_v2) and max(size_v1, size_v2) + 16
	// 	All of them slow the program down... :/
	// TODO: this was not a preallocation, but a pre-fill
	//		=> Need to reinvestiguate this claim.
	std::vector<sparse_mat_elem> v_res; //(std::max(size_v1, size_v2) + 16);

	while (1) {

		// Termination condition : if one of the vectors is done
		if (i1==size_v1) {
			return v_res;
		}
		if (i2==size_v2) {
			return v_res;
		}

		// We have the garanty that there are still elements on both sides

		// Get the two fist elements
		struct sparse_mat_elem e1 = v1[i1];
		struct sparse_mat_elem e2 = v2[i2];

		if (e1.ind_col < e2.ind_col) {
			// No conflict - e1 first
			i1++;
		} else if (e1.ind_col > e2.ind_col) {
			// No conflict - e2 first
			i2++;
		} else {
			// e1.ind_col = e2.ind_col

			// Confict => we use the combination operator here
			unsigned short int nval = std::min(e1.val, e2.val);
			struct sparse_mat_elem e12 = { e1.ind_col, nval };
			v_res.push_back(e12);
			i1++;
			i2++;
		}
	}
}


// Compute the number of different columns across 2 sparse rows
int card_nnz_combined(std::vector<sparse_mat_elem> v1, std::vector<sparse_mat_elem> v2) {

	// Algo de base (sans vectorisation/optimisation)
	unsigned int size_v1 = v1.size();
	unsigned int size_v2 = v2.size();

	// Where in the vectors v1/v2 we are
	unsigned int i1 = 0;
	unsigned int i2 = 0;

	// Resulting vector
	// Pre-allocations tested: max(size_v1, size_v2) and max(size_v1, size_v2) + 16
	// 	All of them slow the program down... :/
	// TODO: this was not a preallocation, but a pre-fill
	//		=> Need to reinvestiguate this claim.
	int card = 0;
	while (1) {

		// Termination condition : if one of the vectors is done
		if (i1==size_v1) {
			return card + (size_v2 - i2);
		}
		if (i2==size_v2) {
			return card + (size_v1 - i1);
		}

		// We have the garanty that there are still elements on both sides

		// Get the two fist elements
		struct sparse_mat_elem e1 = v1[i1];
		struct sparse_mat_elem e2 = v2[i2];

		if (e1.ind_col < e2.ind_col) {
			card++;
			i1++;
		} else if (e1.ind_col > e2.ind_col) {
			card++;
			i2++;
		} else {
			card++;
			i1++;
			i2++;
		}
	}
}

int card_nnz(std::vector<sparse_mat_elem> v1) {
	return v1.size();
}


// Optim 1 : vector => tableaux (tested: bof)
// Optim 2 : vectorization des lignes (not needed)


// ============================================


// === Basic operation 2: transposing a sparse matrix ===
struct sparse_mat transpose_sparse_mat(struct sparse_mat mat) {
	// Preping the list of list of elements
	std::vector<std::vector<sparse_mat_elem>> vvtr_elem;
	for (unsigned short int j=0; j<mat.num_col; j++){
		std::vector<sparse_mat_elem> vtr_elem;
		vvtr_elem.push_back(vtr_elem);
	}

	for (unsigned short int i=0; i<mat.num_row; i++) {
		std::vector<sparse_mat_elem> mat_row = mat.elements[i];

		for (sparse_mat_elem elem : mat_row) {
			struct sparse_mat_elem nelem =  { i, elem.val };
			vvtr_elem[elem.ind_col].push_back(nelem);
		}
	}

	// Wrapping up (while permuting num_col and num_row)
	struct sparse_mat trmat { mat.num_col, mat.num_row, vvtr_elem};
	return trmat;
}

