
#include "reorder_Gre_heuristic.hpp"


// ============== Tests from sparse_vector_combi.cpp ==============

// Testing combine - combine_add_vector_rows
void test_combine_add_vector_rows_simple() {
	std::vector<unsigned short int> v1_indcol = {0, 3, 4, 6, 7, 8, 9};
	std::vector<sparse_mat_elem> v1test = create_sparse_vector(v1_indcol);

	std::vector<unsigned short int> v2_indcol = {1, 3, 4, 5, 7};
	std::vector<sparse_mat_elem> v2test = create_sparse_vector(v2_indcol);

	std::vector<sparse_mat_elem> v12test = combine_add_vector_rows(v1test, v2test);

	print_sparse_vector(v12test);
	// [ (0,1) (1,1) (3,2) (4,2) (5,1) (6,1) (7,2) (8,1) (9,1) ]

	assert(v12test.size() == 9);

	return;
}

// Testing combine - combine_add_vector_rows
void test_combine_card_nnz_combined() {
	std::vector<unsigned short int> v1_indcol = {0, 3, 4, 6, 7, 8, 9};
	std::vector<sparse_mat_elem> v1test = create_sparse_vector(v1_indcol);

	std::vector<unsigned short int> v2_indcol = {1, 3, 4, 5, 7};
	std::vector<sparse_mat_elem> v2test = create_sparse_vector(v2_indcol);

	int cardnnzcomb = card_nnz_combined(v1test, v2test);

	printf("%d\n", cardnnzcomb);
	// Supposed to be 9

	assert(cardnnzcomb == 9);

	return;
}


// Auxilliary function: perform N^2 calls to combine_add_vector_rows
//		to stress-test the performance of this function
void test_combine_add_all_couple(struct sparse_mat spmat_test) {
	// Clock measurement
	using std::chrono::high_resolution_clock;
	using std::chrono::duration_cast;
	using std::chrono::duration;
	using std::chrono::milliseconds;
	auto t1 = high_resolution_clock::now();

	int dummy = 0;

	// For each couple of rows in spmat_test,
	//	we apply the "combine_add_vector_rows" function
#pragma omp parallel for
	for (unsigned short int i=0; i<spmat_test.num_row; i++) {

	/* Test de timing (pour observer la vitesse sans tout executer)
	if (i % 32 == 0) {
		std::cout << "test_combine_add_all_couple :: Ligne " << i << " reached\n";
	}
	//*/

	for (unsigned short int j=0; j<spmat_test.num_row; j++) {
		// Note: want to include "i=j" here

		std::vector<sparse_mat_elem> v1 = spmat_test.elements[i];
		std::vector<sparse_mat_elem> v2 = spmat_test.elements[j];

		std::vector<sparse_mat_elem> v3 = combine_add_vector_rows(v1, v2);

		// To avoid compiler optimization
		if (v3.empty())
			dummy++;

		// DEBUG
		//print_sparse_vector(v1);
		//print_sparse_vector(v2);
		//print_sparse_vector(v3);
		//std::cout << "\n";

	}
	}

	auto t2 = high_resolution_clock::now();
	auto ms_int = duration_cast<milliseconds>(t2 - t1);
	std::cout << ms_int.count() << "ms\n";

	std::cout << "(dummy var to prevent compiler optimization " << dummy << ")\n";

	return;
}


// Testing combine ("We need a bigger file!")
void test_combine_add_vector_rows_big_matrix() {
	std::string filename = "../../sparse_mat/matrix_test.mtx";
	//std::string filename = "../../sparse_mat/sparsemat_32_32.mtx";
	//std::string filename = "../../sparse_mat/sparse_mat_BIG_2.mtx";

	struct sparse_mat spmat_test = custom_parse_mtx_file(filename);
	//std::cout << "Parsing of matrix successful\n";
	//for (int i=0;i<spmat_test.num_row; i++) {
	//	print_sparse_vector(spmat_test.elements[i]);
	//}
	test_combine_add_all_couple(spmat_test);

	return;
}


// Test permutation
void test_permutation() {
	std::string filename = "../../sparse_mat/sparsemat_32_32.mtx";
	//std::string filename = "../../sparse_mat/sparse_mat_BIG_2.mtx";
	
	struct sparse_mat spmat_test = custom_parse_mtx_file(filename);
	//print_sparse_mat(spmat_test);

	struct sparse_mat tr_sparse_mat = transpose_sparse_mat(spmat_test);
	//print_sparse_mat(tr_sparse_mat);

	// To avoid compilation optim
	print_sparse_vector(tr_sparse_mat.elements[0]);

	return;
}


// Main function for the tests related to "sparse_vector_combi.cpp"
void main_test_sparse_vector_combi() {
	test_combine_add_vector_rows_simple();
	test_combine_card_nnz_combined();

	test_combine_add_vector_rows_big_matrix();
	test_permutation();

	return;
}

// ==================================================
// ==================================================
// ==================================================

// ============== Tests from reorder_Gre_heuristic.cpp ==============

// Test permute on a 4*4 matrix
void test_permute_4x4() {
	std::string filename = "../../sparse_mat/matrix_test.mtx";
	struct sparse_mat spmat_test = custom_parse_mtx_file(filename);

	std::vector<unsigned short int> permut_rows = {1,0,3,2}; //{0,1,2,3};
	std::vector<unsigned short int> permut_cols = {1,0,3,2}; //{0,1,2,3};

	struct sparse_mat spmat_test_permuted = permute(spmat_test, permut_rows, permut_cols, true);
	print_sparse_mat(spmat_test_permuted);

	return;
}


// Test permute on a 8*8 matrix
void test_permute_8x8() {
	std::string filename = "../../sparse_mat/debug_8_8.mtx";
	struct sparse_mat spmat_test = custom_parse_mtx_file(filename);
	print_sparse_mat(spmat_test);

	std::vector<unsigned short int> permut_rows = {0, 1, 2, 4, 3, 5, 6, 7};
	std::vector<unsigned short int> permut_cols = {0, 1, 2, 7, 3, 4, 5, 6};

	struct sparse_mat spmat_test_permuted = permute(spmat_test, permut_rows, permut_cols, true);
	print_sparse_mat(spmat_test_permuted);

	return;
}


// Test of "fused"
void test_fused() {
	std::string filename = "../../sparse_mat/matrix_test.mtx";
	struct sparse_mat spmat_test = custom_parse_mtx_file(filename);
	print_sparse_mat(spmat_test);
	std::cout << "\n";

	std::vector<unsigned short int> permut_rows = {3,2,1,0};// {1,0,3,2}; //{0,1,2,3};
	std::vector<unsigned short int> permut_cols = {1,0,3,2}; //{0,1,2,3};

	struct sparse_mat spmat_test_fused = fused(spmat_test, permut_rows, permut_cols, 2, 2);
	print_sparse_mat(spmat_test_fused);

	return;
}

// Test of the cost function (unittest taken from Valentin's Python side)
void test_cost_func() {
	unsigned short int blocksize_i = 2;
	unsigned short int blocksize_j = 3;
	int threshold = 2;

	unsigned short int num_row = 6;
	unsigned short int num_col = 9;
	std::vector<std::vector<double>> vvelem_double(num_row);
	std::vector<double> velem0 = {1, 1, 1, 0, 1, 0, 1, 0, 0};
	vvelem_double[0] = velem0;
	std::vector<double> velem1 = {1, 1, 0, 0, 0, 0, 0, 0, 0};
	vvelem_double[1] = velem1;
	std::vector<double> velem2 = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	vvelem_double[2] = velem2;
	std::vector<double> velem3 = {0, 0, 0, 1, 1, 1, 1, 0, 1};
	vvelem_double[3] = velem3;
	std::vector<double> velem4 = {0, 0, 0, 0, 1, 0, 0, 0, 1};
	vvelem_double[4] = velem4;
	std::vector<double> velem5 = {1, 0, 0, 1, 0, 0, 0, 0, 0};
	vvelem_double[5] = velem5;

	std::vector<std::vector<sparse_mat_elem_double>> vvelem(num_row);
	for (unsigned short int i=0; i<num_row; i++) {
		for (unsigned short int j=0; j<num_col; j++) {
			if (vvelem_double[i][j] != 0.0) {
				struct sparse_mat_elem_double nelem {j, vvelem_double[i][j]};
				vvelem[i].push_back(nelem);
			}
		}
	}

	struct sparse_mat_double test_mat {num_row, num_col, vvelem};

	int nnz_mat = 0;
	for (std::vector<sparse_mat_elem_double> vrow : test_mat.elements)
		nnz_mat += vrow.size();

	int expected = 12;
	int cost = cost_func(test_mat, blocksize_i, blocksize_j, threshold);
	assert(cost <= nnz_mat);
    assert(cost == expected);

    std::cout << "test_cost_func passed!\n";
    return;
}


// Test reordered_algo on a 4*4 matrix and 2*2 blocks
void test_reordered_4x4_2x2() {
	std::string filename = "../../sparse_mat/matrix_test.mtx";
	struct sparse_mat_double spmat_test_double = custom_parse_mtx_file_double(filename);
	print_sparse_mat_double(spmat_test_double);
	std::cout << "\n";

	struct result_triple res_mat = reordered_algo(spmat_test_double, 2, 2);
	print_sparse_mat_double(res_mat.perm_matrix);

	return;
}


// Test reordered_algo on a 32*32 matrix and 4*4 blocks
// Matrices are generate by Valetin (blocks already there)
//	Thus, we permute them randomly beforehand
void test_reordered_32x32_4x4() {
	std::string filename = "../../sparse_mat/sparsemat_32_32.mtx";
	struct sparse_mat_double spmat_test_double = custom_parse_mtx_file_double(filename);
	print_ascii_nnz_mat_double(spmat_test_double);
	std::cout << "\n";

	// Mix things around
	std::vector<unsigned short int> permut_rows = random_permut(spmat_test_double.num_row);
	std::vector<unsigned short int> permut_cols = random_permut(spmat_test_double.num_col);
	struct sparse_mat_double spmat_mixed_double =
		permute_double(spmat_test_double, permut_rows, permut_cols, false);
	print_ascii_nnz_mat_double(spmat_mixed_double);
	std::cout << "\n";

	unsigned short int blocksize_i = 4;
	unsigned short int blocksize_j = 4;
	struct result_triple res_mat = reordered_algo(spmat_mixed_double, 4, 4);
	print_ascii_nnz_mat_double(res_mat.perm_matrix);
	print_permut(res_mat.permut_rows);
	print_permut(res_mat.permut_cols);

	int threshold = 4;

	int score_start = cost_func(spmat_test_double, blocksize_i, blocksize_j, threshold);
	int score_perm = cost_func(spmat_mixed_double, blocksize_i, blocksize_j, threshold);
	int score_final = cost_func(res_mat.perm_matrix, blocksize_i, blocksize_j, threshold);

	std::cout << "score_start = " << score_start <<
		" | score_perm = " << score_perm <<
		" | score_final = " << score_final << "\n";

	return;
}


// Test reordered_algo on the big matrix and 16*16 blocks
// Matrices are generate by Valetin (blocks already there)
//	Thus, we permute them randomly beforehand
void test_reordered_big_16x16() {
	// "../../sparse_mat_BIG_2.mtx" is a 256*256 random block matrix
	//			 generated by Valentin's script
	//#S = sparsegen.generate_sparse_by_blocks_plus_noise([256,256], 0.05,
	//#		blocks_shape=[16,16], block_density=0.1, inter_block_density=0.8)
	std::string filename = "../../sparse_mat/sparse_mat_256.mtx";

	// "../../sparse_mat_BIG_2.mtx" is a 6144*6144 random block matrix
	//			 generated by Valentin's script
	// (due to its size, we did not include it in the git)
	//#S = sparsegen.generate_sparse_by_blocks_plus_noise([6144,6144], 0.1,
	//#		blocks_shape=[16,16], block_density=0.03, inter_block_density=0.8)
	//std::string filename = "../../sparse_mat/sparse_mat_BIG_2.mtx";
	
	struct sparse_mat_double spmat_test_double = custom_parse_mtx_file_double(filename);
	//print_sparse_mat_double(spmat_test_double);
	//std::cout << "\n";

	// Mix things around
	std::vector<unsigned short int> permut_rows = random_permut(spmat_test_double.num_row);
	std::vector<unsigned short int> permut_cols = random_permut(spmat_test_double.num_col);
	struct sparse_mat_double spmat_mixed_double =
		permute_double(spmat_test_double, permut_rows, permut_cols, true);


	unsigned short int blocksize_i = 16;
	unsigned short int blocksize_j = 16;
	struct result_triple res_mat = reordered_algo(spmat_mixed_double, blocksize_i, blocksize_j);

	//print_sparse_mat_double(res_mat.perm_matrix);
	//print_permut(res_mat.permut_rows);
	//print_permut(res_mat.permut_cols);

	int threshold = 20;

	int score_start = cost_func(spmat_test_double, blocksize_i, blocksize_j, threshold);
	int score_perm = cost_func(spmat_mixed_double, blocksize_i, blocksize_j, threshold);
	int score_final = cost_func(res_mat.perm_matrix, blocksize_i, blocksize_j, threshold);

	std::cout << "score_start = " << score_start <<
		" | score_perm = " << score_perm <<
		" | score_final = " << score_final << "\n";

	return;
}


// Main function for the tests related to "reorder_Gre_heuristic.cpp"
void main_test_Gre_heuristic() {
	//test_permute_4x4();
	//test_permute_8x8();
	//test_fused();
	//test_cost_func();

	// Tests of the main "reordered" function
	//test_reordered_4x4_2x2();
	//test_reordered_32x32_4x4();
	test_reordered_big_16x16();

	return;
}




// ==================================================
// ==================================================
// ==================================================

// ============== Main functions ==============



// Given a list of matrices (specified by their filename and name)
//   compute the score of these matrices and print them out
void cost_function_comparator(std::vector<std::string> vfilename, std::vector<std::string> vname,
		unsigned short int blocksize_i, unsigned short int blocksize_j, int threshold) {
	assert(vfilename.size() == vname.size());
	int nfilename = vfilename.size();

	// Parse the matrices and compute their scores
	std::vector<int> vscore;
	for (std::string filename : vfilename) {
		struct sparse_mat_double spmat = custom_parse_mtx_file_double(filename);
		int score = cost_func(spmat, blocksize_i, blocksize_j, threshold);
		vscore.push_back(score);
	}

	// Print-out the result
	for (int i=0; i<nfilename; i++) {
		int score = vscore[i];
		std::string name_mat = vname[i];

		std::cout << "score_" << name_mat << " = " << score << "\n";
	}

	return;
}


// Testing function (example of usage) for cost_function_comparator
void test_cost_function_comparator() {
	std::string filename_1 = "../../nemeth16_doc/nemeth16.mtx";
	std::string filename_2 = "../../nemeth16_doc/nemeth16_heurGre_V23_02_02.mtx";
	std::string filename_3 = "../../nemeth16_doc/result_python_nemeth16_V23_02_02.mtx";

	std::vector<std::string> vfilename(3);
	vfilename[0] = filename_1;
	vfilename[1] = filename_2;
	vfilename[2] = filename_3;

	std::vector<std::string> vname(3);
	vname[0] = "start";
	vname[1] = "cpp";
	vname[2] = "py";

	unsigned short int blocksize_i = 16;
	unsigned short int blocksize_j = 16;
	int threshold = 20;

	cost_function_comparator(vfilename, vname, blocksize_i, blocksize_j, threshold);

	return;
}


// Given a matrix, apply the algorithm, to try to form 16*16 blocks
void real_matrix_reordered_16x16(std::string filename) {
	struct sparse_mat_double spmat_test_double = custom_parse_mtx_file_double(filename);

	unsigned short int blocksize_i = 16;
	unsigned short int blocksize_j = 16;
	struct result_triple res_mat = reordered_algo(spmat_test_double, blocksize_i, blocksize_j);

	// Save the resulting matrix
	//save_sparse_mat_double(res_mat.perm_matrix, "nemeth16_doc/nemeth16_heurGre.mtx");

	int threshold = 20;
	int score_start = cost_func(spmat_test_double, blocksize_i, blocksize_j, threshold);
	int score_final = cost_func(res_mat.perm_matrix, blocksize_i, blocksize_j, threshold);
	std::cout << "score_start = " << score_start <<
			" | score_final = " << score_final << "\n";

	return;
}


// Given a matrix, apply the algorithm, to try to form 4*1 blocks
void real_matrix_reordered_4x1(std::string filename) {
	struct sparse_mat_double spmat_test_double = custom_parse_mtx_file_double(filename);

	unsigned short int blocksize_i = 4;
	unsigned short int blocksize_j = 1;
	struct result_triple res_mat = reordered_algo(spmat_test_double, blocksize_i, blocksize_j);

	// Save the resulting matrix
	//save_sparse_mat_double(res_mat.perm_matrix, "nemeth16_doc/nemeth16_heurGre.mtx");

	int threshold = 1;
	int score_start = cost_func(spmat_test_double, blocksize_i, blocksize_j, threshold);
	int score_final = cost_func(res_mat.perm_matrix, blocksize_i, blocksize_j, threshold);
	std::cout << "score_start = " << score_start <<
			" | score_final = " << score_final << "\n";

	return;
}


// Testing function (example of usage) for real_matrix_reordered_16x16
void test_real_matrix_reordered_16x16() {
	//std::string filename = "../../nemeth16_doc/nemeth16.mtx";
	//std::string filename = "../../sparse_mat/case9_A_03.mtx";
	std::string filename = "../../sparse_mat/heart2.mtx";

	real_matrix_reordered_16x16(filename);

	return;
}


// Main function
int main() {
	// Tests (sorted per C++ file)
	//main_test_sparse_vector_combi();
	//main_test_Gre_heuristic();


	// Additional tests of the "interesting" functions
	//test_cost_function_comparator();
	//test_real_matrix_reordered_16x16();

	// Expe between different affinity function
	//real_matrix_reordered_4x1("../../nemeth16_doc/nemeth16.mtx");
	//real_matrix_reordered_4x1("../../sparse_mat/ACTIVSg10K.mtx");
	real_matrix_reordered_4x1("../../sparse_mat/resnet_v1_50_88.mtx");
	//real_matrix_reordered_4x1("../../sparse_mat/bcsstk34.mtx");
	//real_matrix_reordered_4x1("../../sparse_mat/wiki-RfA.mtx");

	return 0;
}

