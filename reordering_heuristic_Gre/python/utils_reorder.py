import matplotlib.pyplot as plt
import numpy as np
from nptyping import Shape, NDArray, Int16, Number

Matrix = NDArray[Shape["N,M"], Number]
VecInt = NDArray[Shape["N"], Int16]
MatInt = NDArray[Shape["N,M"], Int16]


def prime_factors(number: int) -> list[int]:
    """
    Compute the prime factors and their power of a number.
    Example:`prime_factors(60) -> [2, 2, 3, 5] (60 = 2 * 2 * 3 * 5)`
    """
    if number < 2:
        return []
    max_i = int(np.ceil(np.sqrt(number)))
    # init case with 2
    i = int(2)
    res = []
    while (number & 1) == 0:
        number //= i
        res.append(i)
    # now only odd numbers left so we can step 2 by 2
    for i in range(3, max_i + 2, 2):
        if number == 1:
            break
        while (number != 1) and (number % i) == 0:
            number //= i
            res.append(i)

    return res


def plot_sparse_matrices(matrices: list[tuple[Matrix, str]], save: str = ""):
    fig, axs = plt.subplots(nrows=1, ncols=len(matrices), figsize=(16, 9))
    for (mat, title), ax in zip(matrices, axs.flatten()):
        plt.sca(ax)
        plt.spy(mat)
        plt.title(title)

    if save:
        fig.savefig(save)  # pyright: ignore
    else:
        plt.show()


def cost_func(sparse: Matrix, blocsize: tuple[int, int], threshold: int) -> int:
    assert sparse.shape >= blocsize
    matrix01: MatInt = np.int16(sparse > 0.0)  # pyright: ignore
    ni, nj = matrix01.shape
    bi, bj = blocsize
    cost = sum(
        (
            min(int(np.sum(matrix01[i : i + bi, j : j + bj])), threshold)
            for i in range(0, ni, bi)
            for j in range(0, nj, bj)
        )
    )
    return cost


def cost_min_theoritical(
    sparse: Matrix, blocsize: tuple[int, int], threshold: int, nnz: int | None = None
) -> int:
    assert sparse.shape >= blocsize
    if nnz is None:
        nnz = np.count_nonzero(sparse)
    bi, bj = blocsize
    nfilled, nrest = divmod(nnz, bi * bj)
    return threshold * nfilled + min(threshold, nrest)


def permute(matrix: Matrix, permut_rows: VecInt, permut_cols: VecInt) -> Matrix:
    """
    Return a new matrix with line swapped according to perm_rows and columns swapped according to permuted_cols.
    """
    permuted = matrix.copy()
    for new_i, old_i in enumerate(permut_rows):
        for new_j, old_j in enumerate(permut_cols):
            permuted[new_i, new_j] = matrix[old_i, old_j]
    return permuted


def fused(
    matrix: MatInt,
    permutations: tuple[VecInt, VecInt],
    blocsize: tuple[int, int],
) -> MatInt:
    """
    Return a new matrix of shape N/blocsize_i x M/blocsize_j (matrix's shape is NxM)
    with elements fused[i,j] = sum(matrix_block[i,j])
    where matrix_block take care of the given permutations
    i.e we want the number of nonzeros in the block obtained after permutation.
    """
    n, m = matrix.shape
    blocsize_i, blocsize_j = blocsize
    perm_i, perm_j = permutations
    fused_shape = (n // blocsize_i, m // blocsize_j)
    res = np.zeros(fused_shape, dtype=np.int16)
    for i in range(fused_shape[0]):
        for j in range(fused_shape[1]):
            bloc_i = blocsize_i * i
            bloc_j = blocsize_j * j
            res[i, j] = sum(
                (
                    matrix[perm_i[new_i], perm_j[new_j]]
                    for new_i in range(bloc_i, bloc_i + blocsize_i)
                    for new_j in range(bloc_j, bloc_j + blocsize_j)
                )
            )

    return res
