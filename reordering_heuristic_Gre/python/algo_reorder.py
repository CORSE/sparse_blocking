"""
This script is here to test implementaton of the reordering algorithm proposed by Saday and see if we can improve it.
"""
# import time
from itertools import zip_longest
import os
from typing import Callable, Iterable
import numpy as np
from scipy.io import mmread, mmwrite
from scipy.sparse import coo_matrix

# from scipy.sparse import csr_matrix, diags
from sparsegen import generate_sparse_by_blocks_plus_noise, RNG
from utils_reorder import (
    prime_factors,
    cost_func,
    permute,
    fused,
    plot_sparse_matrices,
    cost_min_theoritical,
    VecInt,
    MatInt,
    Matrix,
)


def new_permutation(
    affinities: list[list[tuple[np.int16, int]]],
    permutation: VecInt,
    nblocs: int,
    blocsize: int,
    p: int = 2,
) -> VecInt:
    """
    Return a new permutation on the axis of your choice.

    # Warning
    This code is common for row and column and should be used with arguments from one dimension dont mix column with row related data.
    """
    # sort affinities so last element is the best match
    for lst in affinities:
        lst.sort()

    def get_best(itera: Iterable) -> tuple[int, int, np.int16]:
        # greedy take max affinity from current singles
        i1 = max(itera, key=lambda i: affinities[i][-1])
        aff, i2 = affinities[i1].pop()
        while i2 not in singles:
            i1 = max(itera, key=lambda i: affinities[i][-1])
            aff, i2 = affinities[i1].pop()
        return i1, i2, aff

    # build set of p-uplets with better affinity
    singles = {i for i in range(nblocs)}
    res_p = np.zeros(len(permutation), dtype=np.int16)
    idx = 0
    while singles:
        cur_i, i2, aff = get_best(singles)
        # update singles and affinities
        affinities[i2].remove((aff, cur_i))
        singles.remove(cur_i)
        singles.remove(i2)
        puplet = [cur_i, i2]
        # complete the group with the best new candidate
        # i.e the one with the highest affinity
        while len(puplet) != p:
            i, i3, aff = get_best(puplet)
            affinities[i3].remove((aff, i))
            puplet.append(i3)
            singles.remove(i3)

        # puplet is completed so we can update permutation
        puplet.sort()
        for i in puplet:
            bloc_i = i * blocsize
            for tmp in range(bloc_i, bloc_i + blocsize):
                res_p[idx] = permutation[tmp]
                idx += 1

    return res_p


def affinities_naive(
    fused_matrix: MatInt, axis: int
) -> list[list[tuple[np.int16, int]]]:
    """
    Compute array of affinty between 2 rows or 2 columns (choosen by axis).

    The output is non sorted and contains tuples like (affinity_value, j2) for columns affinities.
    """
    nblocs = fused_matrix.shape[axis]
    affinities = [
        [(np.int16(-1), int(-1)) for _ in range(nblocs)] for _ in range(nblocs)
    ]
    for j1 in range(nblocs):
        for j2 in range(j1):
            row1 = np.take(fused_matrix, j1, axis)
            row2 = np.take(fused_matrix, j2, axis)
            aff = np.sum(np.minimum(row1, row2))
            affinities[j1][j2] = (aff, j2)
            affinities[j2][j1] = (aff, j1)

    for j in range(nblocs):
        affinities[j].pop(j)  # remove the diagonal with sentinels

    return affinities


def merge_puplets(
    matrix: MatInt,
    permutations: tuple[VecInt, VecInt],
    blocsize: tuple[int, int],
    axis: int,
    affinities_func: Callable[[MatInt, int], list[list[tuple[np.int16, int]]]],
    p: int = 2,
) -> VecInt:
    """
    Return a new permutation for a given axis that maximise affinity between blocks.

    # Warning
    It assume a matrix with a even number of blocs on the given axis and will panic with assertion error if not.
    """
    nblocs = matrix.shape[axis] // blocsize[axis]
    # compute fused matrix F
    fused_matrix = fused(matrix, permutations, blocsize)

    # compute affinity for every j1 =! j2
    # affinity = sum(I=0, I=n/blocsize_i, min(F[I,j1], F[I,j2]))
    # similar for line
    affinities = affinities_func(fused_matrix, axis)

    return new_permutation(affinities, permutations[axis], nblocs, blocsize[axis], p)


def reordered_algo(
    matrix: Matrix,
    blocsize: tuple[int, int],
    func: Callable[[MatInt, int], list[list[tuple[np.int16, int]]]] = affinities_naive,
) -> tuple[Matrix, VecInt, VecInt]:
    """
    Return a permuted matrix and its permutations on rows and cols to group nonzeros in blocks of size blocsize_i x blocsize_j.

    # Warning
    Only square blocks are allowed for now if not an assertion error will be raised.
    """
    n, m = matrix.shape
    # convert matrice of float to matrice of 0 if zero and 1 if nonzero
    matrix01: MatInt = np.int16(matrix > 0.0)  # pyright: ignore

    bi, bj = blocsize
    assert bi > 0
    assert bj > 0
    assert n % bi == 0
    assert m % bj == 0
    cur_bi, cur_bj = 1, 1
    perm_i = np.arange(n, dtype=np.int16)
    perm_j = np.arange(m, dtype=np.int16)
    # print(f"Start algo on matrix of shape {n}x{m}")
    # init = time.time()
    for pi, pj in zip_longest(prime_factors(bi), prime_factors(bj)):
        print(f"Cur block {cur_bi}x{cur_bj}")
        print(f"Cur factor {pi}x{pj}")
        if pi is not None:
            perm_i = merge_puplets(
                matrix01,
                (perm_i, perm_j),
                (cur_bi, cur_bj),
                axis=0,
                affinities_func=func,
                p=pi,
            )
            cur_bi *= pi
        if pj is not None:
            perm_j = merge_puplets(
                matrix01,
                (perm_i, perm_j),
                (cur_bi, cur_bj),
                axis=1,
                affinities_func=func,
                p=pj,
            )
            cur_bj *= pj
    # print(f"Algo done in {time.time() - init}s")

    return permute(matrix, perm_i, perm_j), perm_i, perm_j


def aff_test(fused_matrix: MatInt, cur_bloc: int) -> list[list[tuple[np.int16, int]]]:
    nblocs = fused_matrix.shape[0]
    affinities = [
        [(np.int16(-1), int(-1)) for _ in range(nblocs)] for _ in range(nblocs)
    ]
    i16_max_val = np.iinfo(np.int16).max
    for j1 in range(nblocs):
        for j2 in range(j1):
            row1 = np.take(fused_matrix, j1, 0)
            row2 = np.take(fused_matrix, j2, 0)
            aff = i16_max_val - np.sum((cur_bloc - (row1 + row2)) % cur_bloc)
            affinities[j1][j2] = (aff, j2)
            affinities[j2][j1] = (aff, j1)

    for j in range(nblocs):
        affinities[j].pop(j)  # remove the diagonal with sentinels

    return affinities


def reordered_algo_bis(matrix: Matrix, band_len: int) -> Matrix:
    n, m = matrix.shape
    # convert matrice of float to matrice of 0 if zero and 1 if nonzero
    matrix01: MatInt = np.int16(matrix > 0.0)  # pyright: ignore

    assert band_len > 0
    assert n % band_len == 0
    cur_bi = 1
    perm_i = np.arange(n, dtype=np.int16)
    perm_j = np.arange(m, dtype=np.int16)
    # print(f"Start algo on matrix of shape {n}x{m}")
    # init = time.time()
    for pi in prime_factors(band_len):
        print(f"Cur block {cur_bi}")
        print(f"Cur factor {pi}")

        nblocs = n // cur_bi
        # compute fused matrix F
        fused_matrix = fused(matrix01, (perm_i, perm_j), (cur_bi, 1))

        # compute affinity for every j1 =! j2
        # affinity = sum(I=0, I=n/blocsize_i, min(F[I,j1], F[I,j2]))
        # similar for line
        affinities = aff_test(fused_matrix, cur_bi * pi)
        # affinities = affinities_naive(fused_matrix, 0)

        perm_i = new_permutation(affinities, perm_i, nblocs, cur_bi, pi)
        cur_bi *= pi
    # print(f"Algo done in {time.time() - init}s")

    return permute(matrix, perm_i, perm_j)


def try_stuff_with_rng():
    shape = (120, 120)
    blocsize = (6, 1)
    threshold = 10
    matrix = generate_sparse_by_blocks_plus_noise(
        shape,
        0.01,
        blocsize=blocsize,
        block_density=0.05,
        in_block_density=1.0,
    )
    # matrix = diags(
    #     [0.32, 0.54, 0.76], [-1, 0, 1], shape=shape  # pyright:ignore
    # ).toarray()  # pyright:ignore

    # shuffle it by permutation of row and columns
    shuffled = RNG.permutation(matrix, axis=0)
    # shuffled = RNG.permutation(shuffled, axis=1)

    # apply algorithm and see if its back as it was

    # result, _, _ = reordered_algo(shuffled, blocsize)
    result = reordered_algo_bis(shuffled, blocsize[0])

    # display all
    matrices = [
        (matrix, "Expected Matrix"),
        (shuffled, "Shuffled as input"),
        (result, "Output of blocking algorithm"),
    ]
    print("---------------------------------")
    print(f"cost original {cost_func(matrix, blocsize, threshold)}")
    print(f"cost shuffled {cost_func(shuffled, blocsize, threshold)}")
    print(f"cost reordered {cost_func(result, blocsize, threshold)}")
    plot_sparse_matrices(matrices)


def stuff_with_real_matrix():
    path = "../../cpu_threshold_xp/counters_measures/tmpdata"
    path2 = "../../cpu_threshold_xp/counters_measures/reordered"
    bi = 6
    bj = 1
    blocsize = (bi, bj)
    threshold = 2
    files = os.listdir(path)
    if not os.path.exists(path2):
        os.makedirs(path2)
    max_len = 4 + max(map(len, files))
    stats = []
    columns = [
        "file",
        "#rows",
        "#cols",
        "#nonzeros",
        "density",
        "cost_original",
        "cost_algo",
        "cost_min_th",
    ]
    for filename in files:
        if os.path.exists(f"{path2}/{filename}"):
            continue
        print(f"Do stuff on {filename}")
        original = mmread(f"{path}/{filename}")

        n, m = original.shape
        # padding of the shapes by adding zeros to get divisibility with blocksize
        # since mmread return a COO matrix we just change the shape
        original._shape = ((n + bi - 1) // bi * bi, (m + bj - 1) // bj * bj)
        original_dense = original.toarray()

        reordered, _, _ = reordered_algo(original_dense, blocsize)
        nnz = original.nnz
        s = [
            filename,
            n,
            m,
            nnz,
            np.round(nnz / (n * m), 2),
            cost_func(original_dense, blocsize, threshold),
            cost_func(reordered, blocsize, threshold),
            cost_min_theoritical(original_dense, blocsize, threshold),
        ]
        stats.append(s)
        print(s)
        mmwrite(f"{path2}/{filename}", coo_matrix(reordered))
        # max_len = max(max_len, max(map(lambda x: len(str(x)) + 4, s + columns)))

    header = "".join((f"{columns[i]:^{max_len}}" for i in range(len(columns))))
    print(header)
    print("-" * len(header))
    for s in stats:
        line = "".join((f"{s[i]:^{max_len}}" for i in range(len(s))))
        print(line)


if __name__ == "__main__":
    try_stuff_with_rng()
    # stuff_with_real_matrix()
