"""
This script is here to test implementaton of the reordering algorithm proposed by Saday and see if we can improve it.
"""
import numpy as np
from utils_reorder import Matrix

SEED = 42
RNG = np.random.default_rng(SEED)

Float = np.float32


def generate_sparse_by_blocks(
    shape: tuple[int, int],
    *,
    blocsize: tuple[int, int],
    block_density: float,
    in_block_density: float,
) -> Matrix:
    """
    Generate a dense matrix randomly filled by zeros and random floating point numbers between 0.0 and 1.0.

    # Warning:
    It assumes divisibility between the shape provided and the blocks_size. For example, `generate_sparse_by_blocks((8,8), blocks_size=(3,3), ...)` will crash with an assertion error.

    # Arguments:
    - shape (Shape): a tuple of integers representing the MxN size of the output matrix.
    - blocks_shape(Shape): a tuple of integers representing the size of the blocks.
    - block_density (float): rate of nonzero blocks inside the output matrix, between 0.0 and 1.0.
    - inter_block_density (float): rate of nonzeros inside each block, between 0.0 and 1.0.
    """
    assert shape > (0, 0)
    assert blocsize > (0, 0)
    ni, nj = shape
    bi, bj = blocsize
    assert ni % bi == 0
    assert nj % bj == 0
    assert 0.0 <= block_density <= 1.0
    assert 0.0 <= in_block_density <= 1.0
    matrix = np.zeros(shape, dtype=Float)

    n_blocks = ni // bi * nj // bj
    nnz_blocks = int(np.ceil(n_blocks * block_density))
    n_in_block = bi * bj
    nnz_in_block = int(np.ceil(n_in_block * in_block_density))

    blocks_id = [(i, j) for i in range(0, ni, bi) for j in range(0, nj, bj)]
    assert len(blocks_id) == n_blocks
    blocks_id = RNG.choice(blocks_id, size=nnz_blocks, replace=False, shuffle=False)
    possible_in_ids = [(i, j) for i in range(bi) for j in range(bj)]
    for block_i, block_j in blocks_id:
        nnzs_id = RNG.choice(
            possible_in_ids, size=nnz_in_block, replace=False, shuffle=False
        )
        random_floats = RNG.random(size=nnz_in_block, dtype=Float)
        for (i, j), rand in zip(nnzs_id, random_floats):
            matrix[block_i + i, block_j + j] = rand

    return matrix


def generate_sparse(shape: tuple[int, int], density: float) -> Matrix:
    """
    Generate a dense matrix randomly filled by zeros and random floating point numbers between 0.0 and 1.0.

    # Arguments:
    - shape (Shape): a tuple of integers representing the MxN size of the output matrix.
    - density(float): rate of nonzeros, between 0.0 and 1.0.

    """
    assert shape > (0, 0)
    assert 0.0 <= density <= 1.0
    matrix = np.zeros(shape, dtype=Float)
    nnz = int(matrix.size * density)
    indices = RNG.integers(0, matrix.size, size=nnz)
    values = RNG.random(size=nnz, dtype=Float)
    matrix.flat[indices] = values
    return matrix


def generate_sparse_by_blocks_plus_noise(
    shape: tuple[int, int],
    noise_density: float,
    *,
    blocsize: tuple[int, int],
    block_density: float,
    in_block_density: float,
) -> Matrix:
    """
    Generate a dense matrix randomly filled by zeros and random floating point numbers between 0.0 and 1.0.

    # Warning:
    It assumes divisibility between the shape provided and the blocks_size. For example, `generate_sparse_by_blocks_plus_noise((8,8), ,0.5,blocksize=(3,3), ...)` will crash with an assertion error.

    # Arguments:
    - shape (Shape): a tuple of integers representing the MxN size of the output matrix.
    - noise_density(float): rate of nonzeros outside of the block, between 0.0 and 1.0.
    - blocks_shape(Shape): a tuple of integers representing the size of the blocks.
    - block_density (float): rate of nonzero blocks inside the output matrix, between 0.0 and 1.0.
    - inter_block_density (float): rate of nonzeros inside each block, between 0.0 and 1.0.
    """
    matrix = generate_sparse_by_blocks(
        shape,
        blocsize=blocsize,
        block_density=block_density,
        in_block_density=in_block_density,
    )
    noise = generate_sparse(shape, noise_density)
    return matrix + noise
