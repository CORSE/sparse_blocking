"""
This script is here to test implementaton of the reordering algorithm proposed by Saday and see if we can improve it.
"""
from typing import Callable
import pytest
import numpy as np
from algo_reorder import (
    VecInt,
    cost_min_theoritical,
    fused,
    permute,
    reordered_algo,
    cost_func,
)
from sparsegen import Matrix, generate_sparse_by_blocks_plus_noise, RNG


def test_permute_identity():
    mat = np.arange(20, dtype=np.int8).reshape((4, 5))
    perm_i = np.arange(4, dtype=np.int16)
    perm_j = np.arange(5, dtype=np.int16)
    assert np.all(permute(mat, perm_i, perm_j) == mat)


def test_permute_i():
    mat = np.arange(20, dtype=np.int8).reshape((4, 5))
    perm_i = np.array([0, 2, 3, 1], dtype=np.int16)
    perm_j = np.arange(5, dtype=np.int16)
    expected = np.array(
        [[0, 1, 2, 3, 4], [10, 11, 12, 13, 14], [15, 16, 17, 18, 19], [5, 6, 7, 8, 9]],
        dtype=np.int8,
    )
    assert np.all(permute(mat, perm_i, perm_j) == expected)


def test_permute_j():
    mat = np.arange(20, dtype=np.int8).reshape((4, 5))
    perm_i = np.arange(4, dtype=np.int16)
    perm_j = np.array([0, 2, 3, 4, 1], dtype=np.int16)
    expected = np.array(
        [[0, 2, 3, 4, 1], [5, 7, 8, 9, 6], [10, 12, 13, 14, 11], [15, 17, 18, 19, 16]],
        dtype=np.int8,
    )
    assert np.all(permute(mat, perm_i, perm_j) == expected)


def test_permute_ij():
    mat = np.arange(20, dtype=np.int8).reshape((4, 5))
    perm_i = np.array([3, 2, 1, 0], dtype=np.int16)
    perm_j = np.array([0, 2, 3, 4, 1], dtype=np.int16)
    expected = np.array(
        [[15, 17, 18, 19, 16], [10, 12, 13, 14, 11], [5, 7, 8, 9, 6], [0, 2, 3, 4, 1]],
        dtype=np.int8,
    )
    assert np.all(permute(mat, perm_i, perm_j) == expected)


def test_fused_perm_i():
    mat = np.array(
        [
            [1, 1, 1, 0, 1, 0, 1, 0, 0],
            [1, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 1, 0, 1],
            [0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 1, 0, 0, 0, 0, 0],
        ],
        dtype=np.int16,
    )
    perm_i = np.array([0, 2, 5, 1, 3, 4])
    perm_j = np.arange(9, dtype=np.int16)
    expected = np.array(
        [
            [3, 1, 1],
            [3, 1, 0],
            [0, 4, 3],
        ],
        dtype=np.int16,
    )
    assert np.all(fused(mat, (perm_i, perm_j), (2, 3)) == expected)


def test_fused_identity():
    mat = np.array(
        [
            [1, 1, 1, 0, 1, 0, 1, 0, 0],
            [1, 1, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 1, 0, 1],
            [0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 1, 0, 0, 0, 0, 0],
        ],
        dtype=np.int16,
    )
    perm_i = np.arange(6, dtype=np.int16)
    perm_j = np.arange(9, dtype=np.int16)
    expected = np.array(
        [
            [5, 1, 1],
            [0, 3, 2],
            [1, 2, 1],
        ],
        dtype=np.int16,
    )
    assert np.all(fused(mat, (perm_i, perm_j), (2, 3)) == expected)


def test_cost():
    threshold = 2
    blocsize = (2, 3)
    mat = np.array(
        [
            [1, 1, 1, 0, 1, 0, 1, 0, 0],
            [1, 1, 0, 0, 0, 0, 0, 0, 0],
            # min(5,2)  + min(1,2)  +  min(1,2)
            # 2 + 1 + 1 = 4
            [0, 0, 0, 0, 0, 0, 0, 0, 0],
            [0, 0, 0, 1, 1, 1, 1, 0, 1],
            # min(0,2)  + min(3,2)  +  min(2,2)
            # 0 + 2 + 2 = 4
            [0, 0, 0, 0, 1, 0, 0, 0, 1],
            [1, 0, 0, 1, 0, 0, 0, 0, 0],
            # min(1,2)  + min(2,2)  +  min(1,2)
            # 1 + 2 + 1 = 4
        ],
        dtype=np.int16,
    )
    expected = 12
    cost = cost_func(mat, blocsize, threshold)
    assert cost <= np.count_nonzero(mat)
    assert cost == expected


def test_cost_min():
    threshold = 20
    blocsize = (16, 16)
    bi, bj = blocsize
    clean_mat = np.array(
        [
            [1 if i < bi or (i == bi and j < 2) else 0 for j in range(3 * bj)]
            for i in range(10 * bi)
        ]
    )
    # clean mat
    # b b b b b b b b
    # b . . . . . . .
    # . . . . . . . .
    shuffled = RNG.permutation(clean_mat, axis=0)
    shuffled = RNG.permutation(shuffled, axis=1)
    expected = cost_func(clean_mat, blocsize, threshold)
    cost_min = cost_min_theoritical(shuffled, blocsize, threshold)
    assert expected == cost_min
    naive_min = np.count_nonzero(clean_mat) / (bi * bj) * threshold
    assert cost_min >= naive_min


@pytest.mark.parametrize(
    ("algo"),
    (reordered_algo,),
)
def test_reorder_algo(
    algo: Callable[[Matrix, tuple[int, int]], tuple[Matrix, VecInt, VecInt]]
):
    blocsize = (16, 16)
    original = generate_sparse_by_blocks_plus_noise(
        (160, 160),
        0.02,
        blocsize=blocsize,
        block_density=0.05,
        in_block_density=0.9,
    )
    shuffled = RNG.permutation(RNG.permutation(original, axis=0), axis=1)
    reordered, _, _ = algo(shuffled, blocsize)
    threshold = 20
    cost_original = cost_func(original, blocsize, threshold)
    cost_reordered = cost_func(reordered, blocsize, threshold)
    assert cost_reordered <= cost_original
