#! /bin/python3

import argparse
import os
from glob import glob
from os.path import isdir, isfile
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import mmread
from utils_reorder import MatInt, fused


def plot_distrib(blocksize: tuple[int, int], path: str):
    mat: MatInt = np.int16(mmread(path).toarray() > 0.0)  # pyright: ignore
    # plt.spy(mat)
    # plt.show()
    i, j = mat.shape
    id = (
        np.arange(i, dtype=np.int16),
        np.arange(j, dtype=np.int16),
    )
    fus = fused(mat, id, blocksize)
    fus = [s for s in fus.flatten() if s > 0]
    plt.hist(fus)
    plt.xlabel("number of non-zeros in block")
    plt.ylabel("occurences")
    bi,bj = blocksize
    matname = path.split("/")[-1].replace(".mtx", "")
    plt.title(f"NNZ per Block of sizes {bi}x{bj} in {matname}")
    plt.show()


def main():
    parser = argparse.ArgumentParser(
        description="Plot number of nonzeros per bloc in a given matrix."
    )
    parser.add_argument(
        "path",
        help="Path to one .mtx file or directory containing matrix(ces) to analyze.",
    )
    parser.add_argument(
        "--block-i",
        type=int,
        default=6,
        help="Number of rows in a block. (default = %(default)s)",
    )
    parser.add_argument(
        "--block-j",
        type=int,
        default=1,
        help="Number of columns in a block. (default = %(default)s)",
    )
    args = parser.parse_args()
    blocksize = (args.block_i, args.block_j)
    if os.path.isdir(args.path):
        files = glob(os.path.join(args.path, "*"))
    else:
        files = [args.path]
    for mtx in files:
        if not os.path.isfile(mtx):
            continue
        print(f"Plotting distrib of {mtx}")
        plot_distrib(blocksize, mtx)


if __name__ == "__main__":
    main()
