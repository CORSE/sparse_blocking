# sparse_blocking

This project contains the code developped in order to create dense subblocks of
sparse matrices, through permutation of rows and columns.

## Structure

- `cpu_sparse_xp`: contains rust code to benchmark generated C code and try
  various strategies of permutation. It also contains a lot of python scripts
  utils and the mtx files in the **matrices** directory.
- `reordering_heuristic_Gre`: contains our first drafts and some prototyping
  code for permutation algorithm. It contains a C++ implementation made by
  Guillaume Iooss and a python implementation of the same idea by Valentin
  Trophime. There is also some little python utils.
- `requirements.txt`: list of python dependencies to install in your venv. Notes
  that many scripts use features from python3.10 so it should be the minimum
  working version.
- `suitsparse_summary.csv`: a summary extracted manually from the
  [SuiteSparseWebInterface](https://sparse.tamu.edu/) with a filter on rows and
  cols (max 100k).

# Requirements

## Python

Getting started using [pyenv](https://github.com/pyenv/pyenv) +
[pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv). Note that the
command for the shell setup part depend on the shell that you use there might be
differents commands for `fish` or `zsh` verify the commands in the README of
pyenv.

```bash
# install eveything and setup your shell
curl https://pyenv.run | bash # install pyenv (some distributions have special package)
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.bashrc
echo 'command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"' >> ~/.bashrc
echo 'eval "$(pyenv init -)"' >> ~/.bashrc

git clone https://github.com/pyenv/pyenv-virtualenv.git $(pyenv root)/plugins/pyenv-virtualenv
echo 'eval "$(pyenv virtualenv-init -)"' >> ~/.bashrc

# setup the project virtual env
pyenv install 3.11
pyenv virtualenv 3.11 venv-sparse-blocking
pyenv local venv-sparse-blocking
pip install -r requirements.txt
```

## Rust

Simply install [Rust](https://www.rust-lang.org/) and update just in case.

```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
rustup update
```

## Intel MKL and vtune

We use MKL as a baseline to compare against in our experiments and sometimes
vtune can be useful for performance debbuging.

### Archlinux:

```bash
sudo pacman -S intel-oneapi-basekit
```

### Debian/Ubuntu:

Following this guide from the
[official intel documentation](https://www.intel.com/content/www/us/en/docs/oneapi/installation-guide-linux/2024-2/apt.html#GUID-A18C12A3-5A90-4B41-AAAE-0B6EA8F88B2E).

TLDR:

```bash
# download the key to system keyring
wget -O- https://apt.repos.intel.com/intel-gpg-keys/GPG-PUB-KEY-INTEL-SW-PRODUCTS.PUB \
| gpg --dearmor | sudo tee /usr/share/keyrings/oneapi-archive-keyring.gpg > /dev/null

# add signed entry to apt sources and configure the APT client to use Intel repository:
echo "deb [signed-by=/usr/share/keyrings/oneapi-archive-keyring.gpg] https://apt.repos.intel.com/oneapi all main" | sudo tee /etc/apt/sources.list.d/oneAPI.list
sudo apt update
sudo apt install intel-basekit
```
