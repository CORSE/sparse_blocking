fn main() {
    pkg_config::Config::new()
        .probe("mkl-dynamic-ilp64-seq")
        .unwrap();
    println!("cargo:rerun-if-changed=src/debug_perf.c");
    cc::Build::new()
        .file("src/debug_perf.c")
        .flag("-O2")
        .flag("-march=native")
        .compile("debug_perf");
}
