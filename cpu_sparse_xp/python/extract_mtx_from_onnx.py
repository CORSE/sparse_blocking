#! python
"""
Simple script to extract matrices hardcoded in the onnx model file.
"""
import argparse
import numpy as np
import onnx
from onnx import numpy_helper
from scipy.io import mmwrite
from scipy.sparse import coo_matrix


def get_cli_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Extract sparse matrices hardcoded in a model.onnx file"
    )
    parser.add_argument("path", help="path/to/model.onnx")
    parser.add_argument(
        "--threshold-density",
        type=float,
        default=0.5,
        help="Maximum density to be considered sparse. (default=%(default)s)",
    )
    return parser.parse_args()


def main():
    args = get_cli_args()
    model = onnx.load(args.path)
    name = args.path.replace(".onnx", "")

    for i, init in enumerate(model.graph.initializer):
        a = numpy_helper.to_array(init)
        if a.ndim < 2:
            continue
        density = np.count_nonzero(a) / a.size
        if density > args.threshold_density:
            print(f"ignored {density=}")
            continue
        shape = a.shape
        if a.ndim > 2:
            # in onnx from outer to inner: fchw
            # we want: whcf (reverse) -> transpose
            # gather all outer dimensions into one

            a = np.transpose(a)
            others = np.product(shape[1:])
            a = a.reshape((others, shape[0]))

        print(f"{init.name} {shape=} {density=} -> {name}_{i}.mtx")
        mmwrite(f"{name}_{i}.mtx", coo_matrix(a))


if __name__ == "__main__":
    main()
