#! python
"""
This is a script to download matrices from the suitesparse interface.
It requires `pip install ssgetpy`.
"""

import argparse
import os
import shutil
import ssgetpy

# MTX_SELECTION = [
#     "flowmeter0",
#     "bcsstk34",
#     "heart2",
#     "Trec12",
#     "c8_mat11",
#     "human_gene1",
#     "bcsstk27",
#     "dwt_503",
#     "nemeth02",
#     "wiki-RfA",
#     "Goodwin_023",
#     "ACTIVSg10K",
#     "S10PI_n",
#     "s3rmt3m3",
#     "pds10",
# ]


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Script to download matrices from suitesparse.",
    )
    parser.add_argument(
        "--dst",
        default="matrices/suitesparse",
        help="Directory to put the matrices downloaded.",
    )
    parser.add_argument(
        "--rows", nargs=2, type=int, default=[None, None], help="Filter rows size"
    )
    parser.add_argument(
        "--cols", nargs=2, type=int, default=[None, None], help="Filter cols size"
    )
    parser.add_argument(
        "--limit", type=int, default=10, help="Number of matrices to download"
    )
    args = parser.parse_args()
    print(args)
    dst = args.dst
    result = ssgetpy.search(
        dtype="real",
        limit=args.limit,
        rowbounds=tuple(args.rows),
        colbounds=tuple(args.cols),
    )

    for i, matrix in enumerate(result):
        print(f"{matrix.name} ({i}/{args.limit})")
        name = matrix.name
        extracted = f"{dst}/{name}"
        final_mtx = f"{extracted}.mtx"
        if os.path.exists(final_mtx):
            print(f"{name} already downloaded.")
            continue
        matrix.download(format="MM", destpath=dst, extract=True)

        # since the extraction is done in a directory we will flat it to be a file
        assert os.path.isdir(extracted)
        shutil.move(f"{extracted}/{name}.mtx", final_mtx)
        shutil.rmtree(extracted)
        assert not os.path.exists(extracted)


if __name__ == "__main__":
    main()
