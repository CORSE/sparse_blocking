import glob
import os


def convert_to_mtx(src_path: str):
    out_path = f"matrices/{src_path.replace('.smtx', '.mtx')}"
    with open(src_path) as f:
        lines = f.readlines()
    header_mtx = "%%MatrixMarket matrix coordinate real general"
    header_nnz = lines[0].strip().replace(",", "")
    nnz = int(header_nnz.split(" ")[-1])

    # convert their CSR format to mtx usual COO
    nnz_cum_per_i = [int(n) for n in lines[1].strip().split(" ")]
    indices_cols = lines[2].strip().split(" ")
    coordinates = ["" for _ in range(nnz)]
    idx = 0
    for i, (nnz_before, nnz_after) in enumerate(zip(nnz_cum_per_i, nnz_cum_per_i[1:])):
        nnz_row = nnz_after - nnz_before
        for _ in range(nnz_row):
            k = int(indices_cols[idx])
            # we are forced to do the +1 to conform to mtx
            # format with 1 based index like fortran
            coordinates[idx] = f"{i+1} {k+1} 1.0"
            idx += 1

    out_dir = out_path.rsplit("/", 1)[0]
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    with open(out_path, "w") as o:
        newlines = "\n".join([header_mtx, header_nnz, "\n".join(coordinates)])
        o.write(newlines)


def main():
    smtx_files = glob.glob("dlmc/**/*.smtx", recursive=True)
    for i, file in enumerate(smtx_files):
        print(f"Processing {file} ({i}/{len(smtx_files)})")
        convert_to_mtx(file)


if __name__ == "__main__":
    main()
