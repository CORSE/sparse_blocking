#! /bin/python3
import logging
import argparse
from typing import Any
import scipy.sparse as sp
from scipy.io import mmwrite
from numpy.random import default_rng
import numpy as np


def sparse_matrix_by_diag_block(
    m: int,
    n: int,
    bi: int,
    bj: int,
    in_bloc_density: float,
    rng: np.random.Generator,
) -> Any:
    """
    Create a sparse matrix of size `m`x`n` composed of dense sub blocks of size `bi`x`bj`.

    ## Arguments
    - m : int number of rows of Output Matrix.
    - n : int number of columns of Output Matrix.
    - bi : int number of rows of Sub Blocks of Output Matrix.
    - bj : int number of columns of Sub Blocks of Output Matrix.
    - in_bloc_density: float frequency of nonzeros elements in each Sub Block of the Output Matrix.
    """
    assert m >= bi > 0
    assert n >= bj > 0
    assert 0.0 <= in_bloc_density <= 1.0
    assert m % bi == 0
    assert n % bj == 0
    nbi = m // bi
    nbj = n // bj
    nblocs = min(nbi, nbj)
    # random sampling of the dense blocs positions
    diag = [
        sp.random(bi, bj, density=in_bloc_density, random_state=rng)
        for _ in range(nblocs)
    ]
    res = sp.block_diag(diag)
    logging.debug(f"{bi=}, {bj=}, {nblocs=}")
    logging.debug(f"{res.shape=} expected ({m}, {n})")
    if res.shape[0] < m:
        res = sp.vstack([res, np.zeros((m - res.shape[0], n))])
    elif res.shape[1] < n:
        res = sp.hstack([res, np.zeros((m, n - res.shape[1]))])
    res.set_shape((m, n))
    assert res.shape == (m, n)
    return res


def sparse_matrix_by_block(
    m: int,
    n: int,
    bi: int,
    bj: int,
    bloc_density: float,
    in_bloc_density: float,
    rng: np.random.Generator,
) -> Any:
    """
    Create a sparse matrix of size `m`x`n` composed of dense sub blocks of size `bi`x`bj`.

    ## Arguments
    - m : int number of rows of Output Matrix.
    - n : int number of columns of Output Matrix.
    - bi : int number of rows of Sub Blocks of Output Matrix.
    - bj : int number of columns of Sub Blocks of Output Matrix.
    - bloc_density: float frequency of dense Sub Blocks in the Output Matrix.
    - in_bloc_density: float frequency of nonzeros elements in each Sub Block of the Output Matrix.
    """
    assert m >= bi > 0
    assert n >= bj > 0
    assert 0.0 <= bloc_density <= 1.0
    assert 0.0 <= in_bloc_density <= 1.0
    assert m % bi == 0
    assert n % bj == 0
    nbi = m // bi
    nbj = n // bj
    nblocs = nbi * nbj
    nnzblocs = int(np.ceil(nblocs * bloc_density))
    zeros = np.zeros((bi, bj))
    blocs = [[zeros for _ in range(nbj)] for _ in range(nbi)]
    # random sampling of the dense blocs positions
    possible_ijs = [(i, j) for j in range(nbj) for i in range(nbi)]
    selected_ijs = rng.choice(possible_ijs, size=nnzblocs, replace=False, shuffle=False)
    for i, j in selected_ijs:
        rand_nzs = sp.random(bi, bj, density=in_bloc_density, random_state=rng)
        blocs[i][j] = rand_nzs  # pyright: ignore
    res = sp.bmat(blocs)
    logging.debug(f"{bi=}, {bj=}")
    logging.debug(f"blocs shape = {len(blocs)}, {len(blocs[0])}")
    logging.debug(f"{res.shape=} expected ({m}, {n})")
    assert res.shape == (m, n)
    return res


def handle_sparse_cmd(args: argparse.Namespace):
    rng = default_rng(args.seed)
    m, n = args.i, args.j
    densities = np.linspace(args.min_density, args.max_density, args.count)
    for density in densities:
        print(f"Generating random sparse coo matrix with density {density:.5f}")
        # by default uniform distribution in [0,1[ for non zero values.
        s = sp.random(m, n, density=density, format="coo", random_state=rng)
        mmwrite(f"density{density:.5f}.mtx", s)


def add_sparse_parser(subparsers: argparse._SubParsersAction):
    parser = subparsers.add_parser("sparse", help="Generates sparse matrices.")
    parser.add_argument(
        "--min-density",
        type=float,
        default=0.0,
        help="Min density in [0.0 , 1.0) of the linear range of density to generate",
    )
    parser.add_argument(
        "--max-density",
        type=float,
        default=1.0,
        help="Max density in [0.0 , 1.0) of the linear range of density to generate",
    )
    parser.set_defaults(func=handle_sparse_cmd)


def handle_blocks_cmd(args: argparse.Namespace):
    rng = default_rng(args.seed)
    m, n = args.i, args.j
    bi, bj = args.bi, args.bj
    density = args.density_inside_block
    frequencies = np.linspace(args.min_blocks_freq, args.max_blocks_freq, args.count)

    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    for blocks_freq in frequencies:
        print(
            f"Generating {m}x{n} matrix with {blocks_freq*100.0:.1f}% {bi}x{bj} sub blocks dense at {density* 100.0 :.2f}%"
        )
        # by default uniform distribution in [0,1[ for non zero values.
        s = sparse_matrix_by_block(m, n, bi, bj, blocks_freq, density, rng)
        if not args.debug:
            out_file = (
                args.output.replace("%I", str(m))
                .replace("%J", str(n))
                .replace("%BI", str(bi))
                .replace("%BJ", str(bj))
                .replace("%FREQ", f"{blocks_freq:.2f}")
                .replace("%DENS", f"{density:.5f}")
            )
            mmwrite(out_file, s)


def add_block_parser(subparsers: argparse._SubParsersAction):
    parser = subparsers.add_parser("blocks", help="Generates sparse by block matrices.")
    parser.add_argument("bi", type=int, help="I of sub blocks")
    parser.add_argument("bj", type=int, help="J of sub blocks")
    parser.add_argument(
        "--output",
        help="Format of the output files name. (default: %(default)s) ",
        default="mat%Ix%Jblocks%BIx%BJfreq%FREQdens%DENS.mtx",
    )
    parser.add_argument(
        "--density-inside-block",
        type=float,
        default=1.0,
        help="Density in [0.0 , 1.0] of sub blocks (default: %(default)s).",
    )
    parser.add_argument(
        "--min-blocks-freq",
        type=float,
        default=0.01,
        help="Min frequency of dense blocks in [0.0 , 1.0] (default: %(default)s).",
    )
    parser.add_argument(
        "--max-blocks-freq",
        type=float,
        default=1.0,
        help="Max frequency of dense blocks in [0.0 , 1.0] (default: %(default)s).",
    )
    parser.set_defaults(func=handle_blocks_cmd)


def handle_diag_blocks_cmd(args: argparse.Namespace):
    rng = default_rng(args.seed)
    m, n = args.i, args.j
    bi, bj = args.bi, args.bj
    densities = np.linspace(args.min_density, args.max_density, args.count)
    if args.debug:
        logging.basicConfig(level=logging.DEBUG)
    for density in densities:
        print(f"Generating diagonal blocks sparse matrix with density {density:.5f}")
        # by default uniform distribution in [0,1[ for non zero values.
        s = sparse_matrix_by_diag_block(m, n, bi, bj, density, rng)
        if not args.debug:
            mmwrite(f"diag_density{density:.5f}.mtx", s)


def add_diag_block_parser(subparsers: argparse._SubParsersAction):
    parser = subparsers.add_parser(
        "diagonal_blocks", help="Generates diagonal sparse by block matrices."
    )
    parser.add_argument("bi", type=int, help="I of sub blocks")
    parser.add_argument("bj", type=int, help="J of sub blocks")
    parser.add_argument(
        "--min-density",
        type=float,
        default=0.0,
        help="Min density in [0.0 , 1.0) of the linear range of density to generate",
    )
    parser.add_argument(
        "--max-density",
        type=float,
        default=1.0,
        help="Max density in [0.0 , 1.0) of the linear range of density to generate",
    )
    parser.set_defaults(func=handle_diag_blocks_cmd)


def main():
    parser = argparse.ArgumentParser(
        description="Generates .mtx files with random sparse matrices."
    )
    parser.add_argument("i", type=int, help="number of rows of the output matrices")
    parser.add_argument("j", type=int, help="number of columns of the output matrices")
    parser.add_argument(
        "--count",
        type=int,
        help="number of matrices to generate (default: %(default)s)",
        default=1,
    )
    parser.add_argument("--debug", action="store_true", help="Activate debug logs")
    parser.add_argument(
        "--seed",
        type=int,
        help="Set the seed to a known value (default: %(default)s).",
        default=42,
    )
    subparsers = parser.add_subparsers(
        help="Choose the kind of matrices you want to generate.",
        required=True,
    )
    add_block_parser(subparsers)
    add_sparse_parser(subparsers)
    add_diag_block_parser(subparsers)

    args = parser.parse_args()
    if args.func:
        args.func(args)


if __name__ == "__main__":
    main()
