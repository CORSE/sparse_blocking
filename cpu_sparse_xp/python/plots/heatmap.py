"""
Simple script to plot a heatmap of execution time per microkernel sizes ixj.
"""
from typing import Any
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np


HEATMAP_CSV = "results/pinocchio_heatmap.csv"


def heatmap():
    df = pd.read_csv(HEATMAP_CSV)
    print(df)
    i_min = np.min(df["i"].values)
    j_min = np.min(df["j"].values)
    i_max = np.max(df["i"].values)
    j_max = np.max(df["j"].values)
    tmp = plt.subplots()
    list_i = list(range(i_min, i_max + 1))
    list_j = list(range(j_min, j_max + 1))
    ax: Any = tmp[1]  # pyright:ignore
    ax.set_xticks(np.arange(len(list_j)), labels=list_j)
    ax.set_yticks(np.arange(len(list_i)), labels=list_i)
    norm = np.zeros((len(list_i), len(list_j)))
    for _, row in df.iterrows():
        i = int(row["i"])
        j = int(row["j"])
        res = float(row["res"])
        print(i, j, res)
        norm[i - 1, j - 1] = res
        ax.text(j - 1, i - 1, f"{res:.1f}", ha="center", va="center")
    im = ax.imshow(norm)  # pyright:ignore
    print(norm)

    ax.set_xlabel("j")  # pyright:ignore
    ax.set_ylabel("i")  # pyright:ignore
    cbar = ax.figure.colorbar(im, ax=ax)  # pyright:ignore
    cbar.ax.set_ylabel("Peak perf %", rotation=-90)
    plt.show()


if __name__ == "__main__":
    heatmap()
