import json
import argparse
import glob
from typing import Any

from nptyping import NDArray
import matplotlib.pyplot as plt
import numpy as np


LATEX_HEADER = [
    "Matrix",
    "Permuted",
    "Kernel Size",
    "Speedup",
    "ZFR",
]

RENAME = {
    "Bands": "Us - No Permutation",
    "BandsPermPrimes": "Us - First Part Permutation",
    "BandsPermPrimesGorder": "Us - Full Permutation",
}

BARORDER = {
    "MKL": 0,
    "Bands": 1,
    "BandsPermPrimes": 2,
    "BandsPermPrimesGorder": 3,
}


def duration_to_ms(d: dict[str, int]) -> float:
    return 1000 * d["secs"] + d["nanos"] / 1_000_000


def extract_ms(d: dict[str, list[dict]]) -> float:
    gen = (duration_to_ms(r["duration"]) for r in d["runs"])
    return min(gen)


def geometric_mean(array: NDArray) -> float:
    return array.prod() ** (1 / len(array))


class BarsData:
    matrices: list[str]
    implems: dict[str, list[float]]
    speed_reorder_algos: dict[str, NDArray]
    kernels: list[str]
    kernels_p: list[str]
    zerofilling: dict[str, list[float]]

    def __init__(self, path_dir: str, implref: str):
        implems = dict()
        matrices = []
        kernels = []
        kernels_p = []
        zerofilling_p = dict()
        alg_to_impl = dict()
        jsons = glob.glob(f"{path_dir}/**/*.json", recursive=True)
        jsons_err = 0
        for jsonf in jsons:
            print(jsonf)
            try:
                with open(jsonf) as f:
                    jdict = json.load(f)
            except json.decoder.JSONDecodeError:
                jsons_err += 1
                continue
            meta = jdict["metadata"]
            if len(implems) == 0:
                implems.update(((k, []) for k in jdict["data"]))
                zerofilling_p.update((k, []) for k in jdict["data"])
                for a in meta["nnz_bands_permuted"]:
                    k = next((k for k in jdict["data"] if a in k))
                    alg_to_impl[a] = k
            mat = meta["matrix"]
            matrices.append(mat)
            i = meta["ukernel"]["i"]
            j = meta["ukernel"]["j"]
            ip = meta["kernel_perm_i"]
            jp = meta["kernel_perm_j"]
            zerofilling_p["Bands"].append(meta["nnz_bands"] / meta["nnz"])
            for k, nnz in meta["nnz_bands_permuted"].items():
                zerofilling_p[alg_to_impl[k]].append(nnz / meta["nnz"])
            kernels.append(f"I={i} J={j}")
            kernels_p.append(f"I={ip} J={jp}")
            for implem, d in jdict["data"].items():
                ms = extract_ms(d)
                implems[implem].append(ms)

        # gather all mkl lists into one
        mkl_implems = [t for t in implems if "MKL" in t]
        if mkl_implems:
            mkl_times = [
                min((implems[t][i] for t in mkl_implems))
                for i in range(len(jsons) - jsons_err)
            ]
            for t in mkl_implems:
                del implems[t]
            implems["MKL"] = mkl_times

        # band_implems = [t for t in implems if "Band" in t]
        # if band_implems:
        #     band_times = [
        #         min((implems[t][i] for t in band_implems))
        #         for i in range(len(jsons) - jsons_err)
        #     ]
        #     for t in band_implems:
        #         del implems[t]
        #     implems["BandBest"] = band_times

        norm = np.array(implems[implref])

        self.implems = implems
        self.matrices = matrices
        self.kernels = kernels
        self.kernels_p = kernels_p
        self.zerofilling_p = zerofilling_p
        self.speed_reorder_algos = {k: norm / np.array(implems[k]) for k in implems}
        print(implems)
        print(self.speed_reorder_algos)
        print(f"error jsons = {jsons_err}")
        print(zerofilling_p.keys())
        print(self.speed_reorder_algos.keys())

    def plot(self, kind: str):
        if kind == "hist":
            self.plot_hist()
        elif kind == "zf":
            self.plot_zf()
        else:
            self.plot_bar()

    def plot_hist(self):
        n_to_cmp = len(self.implems) - 1
        tmp = plt.subplots(1, n_to_cmp, sharey=True, tight_layout=True)
        axs: Any = tmp[1]  # pyright:ignore
        if n_to_cmp == 1:
            axs = [axs]
        # sort the bar according to an hardcoded order
        names_values = list(self.speed_reorder_algos.items())
        names_values.sort(key=lambda x: BARORDER.get(x[0], len(self.matrices)))
        i = 0
        for impl, speed in names_values:
            if "MKL" in impl:
                continue
            axs[i].hist(speed, bins=50)
            axs[i].set_title(RENAME.get(impl, impl))
            axs[i].set_xlabel("Speedup vs MKL")
            i += 1
        axs[0].set_ylabel("Occurences")
        plt.show()

    def plot_zf(self):
        x, y = [], []
        for k, speedups in self.speed_reorder_algos.items():
            if "MKL" in k:
                continue
            print(k, len(self.zerofilling_p[k]), len(speedups))
            x.extend(self.zerofilling_p[k])
            y.extend(speedups)
        plt.plot(x, y, "bo")
        plt.xlabel("Zero filling rate")
        plt.ylabel("Speedup vs MKL")
        plt.show()

    def plot_bar(self):
        tmp = plt.subplots(layout="constrained")
        ax: plt.Axes = tmp[1]  # pyright:ignore
        matrices = [m.split("/")[-1].replace(".mtx", "") for m in self.matrices]

        width = 1 / (len(self.speed_reorder_algos) + 2)
        x = np.arange(len(self.matrices))

        # sort the bar according to an hardcoded order
        names_values = list(self.speed_reorder_algos.items())
        names_values.sort(key=lambda x: BARORDER.get(x[0], len(self.matrices)))

        for offset, (name, values) in enumerate(names_values):
            label = RENAME.get(name, name)
            ax.bar(x + offset * width, values, width, label=label)

        ax.set_ylabel("Speedup (normalized)", fontsize="xx-large")
        n_implems = len(self.implems)
        ax.set_xticks(
            x + ((n_implems - 1) / 2 * width),
            matrices,
            rotation=90,
            fontsize="xx-large",
        )  # pyright:ignore
        ax.legend(fontsize="xx-large")
        plt.show()

    def print_stats(self):
        for k, values in self.speed_reorder_algos.items():
            print(f"{k} Avg Speedup : {geometric_mean(values):.2f}")
            print(f"{k} Max Speedup : {np.max(values):.2f}")
            print(f"{k} Min Speedup : {np.min(values):.2f}")


def main():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="Plot the final results bars us permutation vs us no permutation vs MKL.",
    )
    parser.add_argument(
        "dir_jsons",
        help="Path to the directory containing all jsons from the bench-suit command.",
    )
    parser.add_argument("--disable-plot", action="store_true", help="disable plot")
    parser.add_argument(
        "--kind", choices=["zf", "hist", "bars"], help="Choose plot kind."
    )
    parser.add_argument(
        "--implref", default="MKL", help="Reference implementation for speedup"
    )

    args = parser.parse_args()
    bars = BarsData(args.dir_jsons, args.implref)

    if not args.disable_plot:
        bars.plot(args.kind)
    bars.print_stats()


if __name__ == "__main__":
    main()
