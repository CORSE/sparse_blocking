#! /bin/python3
import argparse
from nptyping import NDArray, Shape, Float
from typing import Optional
import matplotlib.pyplot as plt
import numpy as np
from scipy.io import mmread


def nblocs_occupied(array: NDArray[Shape["*,*"], Float], bi: int) -> int:
    coords = set()
    iterator = np.nditer(array, flags=["multi_index"])
    for val in iterator:
        if val != 0.0:
            i, k = iterator.multi_index
            coords.add((i // bi, k))
    return len(coords)


def plot_sparsity(path: str, shuffle_rows: Optional[str], cost_band: Optional[int]):
    """
    Show sparsity of a matrix stored in a matrix market (.mtx) file.
    """
    mat = mmread(path)
    nnz = mat.nnz
    print(mat.shape)
    title = path
    if shuffle_rows:
        title += f" Shuffled using {shuffle_rows}"
        with open(shuffle_rows) as f:
            s = f.readline()
        shuffle = list(map(int, s.strip().split(",")))
        len_k = mat.shape[1]
        len_i_pad = len(shuffle)
        mat._shape = (len_i_pad, len_k)
        mat = mat.toarray()
        mat_cpy = np.copy(mat)
        for new_i, old_i in enumerate(shuffle):
            mat[new_i, :] = mat_cpy[old_i, :]
    else:
        mat = mat.toarray()

    if cost_band:
        # this is terribly slow but shouldn't be
        nnz_band = cost_band * nblocs_occupied(mat, cost_band)
        cost = nnz_band / nnz
        print(f"{nnz=} {nnz_band=}")
        print(f"Cost = {cost:.2f}")
    plt.title(title)
    plt.spy(mat)
    plt.show()


def main():
    parser = argparse.ArgumentParser(
        description="Plot the sparsity patterns of given matrice using mtx file"
    )

    parser.add_argument(
        "--cost-band",
        type=int,
        help="If set it will print the cost function of the band considering given size.",
    )
    parser.add_argument("mtxfilepath", help="Path to the mtx file to plot.")
    parser.add_argument(
        "--shuffle-rows",
        help="Path to the csv file representing array of permutation of rows indices (array[new_i] = old_i).",
    )

    args = parser.parse_args()
    plot_sparsity(args.mtxfilepath, args.shuffle_rows, args.cost_band)


if __name__ == "__main__":
    main()
