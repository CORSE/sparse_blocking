# How to run a benchmark ?

1. Add intel stuff to your environments.

```bash
source /opt/intel/oneapi/setvars.sh
```

2. Compile and run the benchmark executor.

```bash
cargo run --release -- benchmark-suit matrices
```

## Documentation of the cli rust tool

If you have any doubt on the arguments expected simply pass `-h` or `--help` add the end of the `cargo run -- bla` command.

At the moment, here are the main subcommands expected by the tool:

- `bench-one`: it expects a path to a mtx file and will try some implementations on it.
- `bench-suit`: it expects a path to a directory with mtx files and will try bruteforcing to find the best kernels and permutations before running the benchmark.

There is other subcommands but they are more for testing/debugging purposes like `test-permutation` or `debug-perf`.
You can get the full list by simply `cargo run -- --help`

### Notes

The default AVX is set to 2 so if you want to try some AVX 512 on a machine that supports it don't forget to add `--avx avx512`.

For debugging, there is the `--debug` flag to put at the start (before the subcommand) to enable debug log.

There is some files that act as cache for this program such as `cache.json` and the `permutations` directory.
If you change the implementation of one permutation algorithm the cache with the old permutation of this algorithm should be cleared else it will continue to return the old permutation.

The main documentation for the code is in the generated doc you can see it by running:

```bash
cargo doc --open
```

## Python scripts

All of these script should be run from this current directory else some path might be broken.

- `download_suite_sparse.py`: Download from the SuiteSparse collection with some filter. Try `-h` for available options to filter.
- `extract_mtx_from_onnx.py`: Parse onnx file to catch initializer that are matrices or can be converted to matrices with a method "im2col" like. It will extract the mtx files in the current directory.
- `gen_random_matrices.py`: Generate random matrices with different proprieties. Try `-h` to discover the subcommands available.
- `smtx_to_mtx.py`: script used in the other script `download_dlmc.sh` to convert a sparse format CSR like to the usual mtx COO like format. It shouldn't be run alone.
- `plots/perm_vs_no_perm.py`: script used to produce figures for the paper (september 2023), hist, bars, correlation perf zero filling. Try `-h` for the subcommands and options.
- `plots/sparsity.py`: Plot the structure of a sparse matrix given as mtx file. It has some options for permutation and computing the number of zerofilling. Try `-h` for more info.
- `plots/heatmap.py`: Plot the heatmap of peak performance by micro kernel size (i,j). It has no option and expect an harcoded path to a csv in results
