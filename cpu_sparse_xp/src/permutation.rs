use crate::{utils::get_perm_cache_or_algo, utils::CooMatrix};
use clap::{Args, ValueEnum};
use log::{debug, info};
use rayon::prelude::*;
use serde::Serialize;
use std::{
    cmp::Ordering,
    collections::{HashMap, HashSet},
    path::PathBuf,
};

/// Specific arguments for the "test-permut" subcommand.
#[derive(Args)]
pub struct ArgsTestPermut {
    pub mat: PathBuf,
    pub band_len: u8,
    pub algo: ReorderingAlgos,
}

/// Strategies for reordering.
#[derive(Debug, Clone, Copy, PartialEq, Eq, ValueEnum, Hash, Serialize)]
pub enum ReorderingAlgos {
    Primes,
    Gorder,
    AffOne,
    PrimesGorder,
}

impl ReorderingAlgos {
    /// Common interface method for executing all reordering algorithms.
    pub fn run(&self, coo: &CooMatrix, band_len: u8, matname: &PathBuf) -> Vec<usize> {
        use ReorderingAlgos::*;
        match self {
            Primes => reorder_prime(coo, band_len),
            PrimesGorder => reorder_primes_georder(coo, band_len, matname),
            Gorder => reorder_gorder(coo, band_len),
            AffOne => reorder_one(coo, band_len),
        }
    }
}

struct Gorder {
    rows: Vec<Vec<usize>>,
}

impl Gorder {
    fn new(pad_i: usize, coo: &CooMatrix) -> Self {
        // list of actives k per row i
        let mut rows: Vec<Vec<usize>> = vec![vec![]; pad_i];
        for &(i, k, _) in coo.triplets.iter() {
            rows[i].push(k);
        }
        // assert(rows.iter().all(|r|r.is_sorted()));
        Self { rows }
    }

    fn affinity(&self, i: usize, j: usize) -> u16 {
        use Ordering::*;
        let ni = self.rows[i].len();
        let nj = self.rows[j].len();
        let mut aff: u16 = 0;
        let (mut idx_i, mut idx_j) = (0, 0);
        while (idx_i < ni) && (idx_j < nj) {
            match self.rows[i][idx_i].cmp(&self.rows[j][idx_j]) {
                Less => {
                    idx_i += 1;
                }
                Equal => {
                    idx_i += 1;
                    idx_j += 1;
                    aff += 1;
                }
                Greater => {
                    idx_j += 1;
                }
            }
        }
        aff
    }

    fn affinities(&self) -> Vec<(usize, usize, u16)> {
        (0..(self.rows.len()))
            .into_par_iter()
            .flat_map_iter(|i| (0..i).map(move |j| (j, i, self.affinity(i, j))))
            .collect()
    }

    /// Main part of the Gorder algorithm use the sliding window to compute what row goes next.
    fn find_permutation(&self, window: usize) -> Vec<usize> {
        // same as in the paper we select the
        // node with max neighbors i.e max nonzeros
        let (start_node, _) = self
            .rows
            .iter()
            .enumerate()
            .max_by_key(|(_, nonzeros_k)| nonzeros_k.len())
            .unwrap();

        let len_i = self.rows.len();
        let mut perm: Vec<usize> = vec![0; len_i];
        perm[0] = start_node;

        debug!("Start affinities");
        let tmp = self.affinities();
        debug!("End affinities");

        let mut affinities: Vec<Vec<u16>> = vec![vec![0; len_i]; len_i];
        for (i, j, a) in tmp {
            affinities[i][j] = a;
            affinities[j][i] = a;
        }

        let mut singles: HashSet<usize> = (0..len_i).collect();
        singles.remove(&start_node);
        for i in 1..len_i {
            let i_start = if i < window { 0 } else { i - window };
            let i_max = *singles
                .iter()
                .max_by_key(|v| (i_start..i).map(|j| affinities[perm[j]][**v]).sum::<u16>())
                .unwrap();

            perm[i] = i_max;
            singles.remove(&i_max);
        }
        perm
    }
}

const DEFAULT_WINDOW: usize = 5;

/// Public lower level call to g order algorithm. It expect a band_len >= 2 else it will panic.
pub fn reorder_gorder(coo: &CooMatrix, band_len: u8) -> Vec<usize> {
    assert!(band_len >= 2);
    let band_len = band_len as usize;
    let pad_i = coo.shape[0].next_multiple_of(band_len);
    // permutation array sig_i[new_i] = old_i
    let nnz01 = Gorder::new(pad_i, coo);
    info!("Gorder Computing permutation");
    let sig_i = nnz01.find_permutation(DEFAULT_WINDOW);
    info!("Permutation found!");
    sig_i
}

/// Public lower level call to primes + g order algorithm. It expect a band_len >= 2 else it will panic.
/// It will try to see if primes as already being computed and is in the permutations directory
/// that is used as a cache.
pub fn reorder_primes_georder(coo: &CooMatrix, band_len: u8, matname: &PathBuf) -> Vec<usize> {
    let band_len_u = band_len as usize;
    let primes_perm = get_perm_cache_or_algo(matname, coo, band_len_u, ReorderingAlgos::Primes);

    let pad_i = coo.shape[0].next_multiple_of(band_len_u);

    let mut rows: Vec<HashSet<usize>> = vec![HashSet::new(); pad_i / band_len_u];
    let gorder_coo = coo.permuted_rows(&primes_perm);
    for &(i, k, _) in gorder_coo.triplets.iter() {
        rows[i / band_len_u].insert(k);
    }
    let gorder = Gorder {
        rows: rows
            .into_iter()
            .map(|hashset| {
                let mut v: Vec<usize> = hashset.into_iter().collect();
                v.sort();
                v
            })
            .collect(),
    };
    let meta_order = gorder.find_permutation(DEFAULT_WINDOW);

    (0..pad_i)
        .map(|i| {
            let old_i = meta_order[i / band_len_u] * band_len_u + (i % band_len_u);
            primes_perm[old_i]
        })
        .collect()
}

/// A greedy approach we compute the affinities once then try to gather the whole band.
/// This approach get worst result in term of zerofilling than the primes approach.
pub fn reorder_one(coo: &CooMatrix, band_len: u8) -> Vec<usize> {
    assert!(band_len >= 2);
    let band_len = band_len as usize;
    let pad_i = coo.shape[0].next_multiple_of(band_len);
    // permutation array sig_i[new_i] = old_i
    let mut sig_i: Vec<usize> = (0..pad_i).collect();
    let nnz01 = FusedMatrix::new(coo, pad_i);
    info!("AffOne Computing permutation");
    let a = nnz01.affinities();
    debug!("Affinities are computed");
    sig_i = nnz01.choose_best_aff(a, sig_i, band_len);
    info!("Permutation found!");
    sig_i
}

/// Our first approach of recursively pairing rows or groups of rows until we get to the expected
/// band_len.
pub fn reorder_prime(coo: &CooMatrix, band_len: u8) -> Vec<usize> {
    assert!(band_len >= 2);
    let band_len = band_len as usize;
    let pad_i = coo.shape[0].next_multiple_of(band_len);
    // permutation array sig_i[new_i] = old_i
    let mut sig_i: Vec<usize> = (0..pad_i).collect();
    let nnz01 = FusedMatrix::new(coo, pad_i);
    let mut fused = nnz01.clone();
    info!("Prime Computing permutation");
    for pi in prime_factors_of(band_len) {
        debug!(
            "Current fused size = {pi}, band len = {} / {band_len}",
            fused.cur_band
        );
        let a = fused.affinities();
        debug!("Affinities are computed");
        sig_i = fused.choose_best_aff(a, sig_i, pi);
        debug!("Permutation choosen");
        fused = nnz01.compact(pi, &sig_i);
    }
    info!("Permutation found!");
    sig_i
}

/// A utility struct to be a faster HashSet of integers.
/// It use less memory by using a bitset strategy.
/// the i'th bit set to 1 means that the element is present.
struct Singles {
    n: usize,
    set: Vec<u64>,
}

impl Singles {
    fn new(n: usize) -> Self {
        let len_set = (n + 63) / 64;
        Self {
            n,
            set: vec![u64::MAX; len_set],
        }
    }

    fn remove(&mut self, i: usize) {
        let idx_set = i / 64;
        let idx_bit = i % 64;
        // 1 everywhere expect i th bit set to 0
        self.set[idx_set] &= !(1 << idx_bit);
    }

    fn contains(&self, i: usize) -> bool {
        let idx_set = i / 64;
        let idx_bit = i % 64;
        (self.set[idx_set] & (1 << idx_bit)) != 0
    }

    fn iter(&self) -> impl Iterator<Item = usize> + '_ {
        (0..self.n).filter(|&i| self.contains(i))
    }

    fn is_empty(&self) -> bool {
        let opt = self.iter().next();
        debug!("Not empty {opt:?}");
        opt.is_none()
    }
}

/// Utility struct for the primes algorithm to accumulate the sum of number of nonzeros when
/// pairing the group of rows.
#[derive(Debug, Clone, PartialEq, Eq)]
struct FusedMatrix {
    rows: Vec<Vec<(usize, u16)>>,
    len_i: usize,
    cur_band: usize,
    len_k: usize,
}

impl FusedMatrix {
    fn new(coo: &CooMatrix, pad_i: usize) -> Self {
        let len_k = coo.shape[1];
        // list of actives k per row i
        let mut rows: Vec<Vec<(usize, u16)>> = vec![vec![]; pad_i];
        for &(i, k, _) in coo.triplets.iter() {
            rows[i].push((k, 1));
        }
        for row in rows.iter_mut() {
            row.sort();
        }
        Self {
            rows,
            cur_band: 1,
            len_i: pad_i,
            len_k,
        }
    }

    fn compact(&self, pi: usize, sig_i: &[usize]) -> Self {
        let cur_band = pi * self.cur_band;
        let len_i = self.len_i / cur_band;
        let rows: Vec<Vec<(usize, u16)>> = (0..len_i)
            .map(|i| {
                let old_i = i * cur_band;
                let mut merges: HashMap<usize, u16> = HashMap::new();
                for &perm_i in sig_i.iter().skip(old_i).take(cur_band) {
                    // accumulate in the local vertical block of size cur_band
                    // carefully apply the permutation
                    for (idx, count) in self.rows[perm_i].iter() {
                        *merges.entry(*idx).or_insert(*count) += *count;
                    }
                }
                let mut merges: Vec<(usize, u16)> = merges.into_iter().collect();
                merges.sort();
                merges
            })
            .collect();

        Self {
            rows,
            cur_band,
            len_i,
            len_k: self.len_k,
        }
    }

    /// We now use a sparse implementation because it is faster in general.
    fn affinity(&self, i: usize, j: usize) -> u16 {
        use Ordering::*;
        let ni = self.rows[i].len();
        let nj = self.rows[j].len();
        let mut aff: u16 = 0;
        let (mut idx_i, mut idx_j) = (0, 0);
        while (idx_i < ni) && (idx_j < nj) {
            let (row_i_col, row_i_count) = self.rows[i][idx_i];
            let (row_j_col, row_j_count) = self.rows[j][idx_j];
            match row_i_col.cmp(&row_j_col) {
                Less => {
                    idx_i += 1;
                }
                Equal => {
                    idx_i += 1;
                    idx_j += 1;
                    aff += row_i_count + row_j_count;
                }
                Greater => {
                    idx_j += 1;
                }
            }
        }
        aff
    }

    /// Compute the affinities between all pairs O(n^2) but using parallel loop.
    fn affinities(&self) -> Vec<(u16, usize, usize)> {
        (0..(self.rows.len()))
            .into_par_iter()
            .flat_map_iter(|i| (0..i).map(move |j| (self.affinity(i, j), j, i)))
            .collect()
    }

    /// Select the permutation according to the affinities `affs`.
    ///
    /// # Warning
    /// This is slow on inputs with big number of rows.
    /// It cannot by paralellized easily so it could easily bottleneck in some matrices like
    /// human_gene0.
    ///
    /// # Ideas for improvement
    /// - use a BTree instead of Vec to remove more effectively the pairs involving one or more
    /// member of the completed p-uplet.
    /// - duplicate data to be able to iterate in descending affinity and also access pairs per row
    /// index.
    fn choose_best_aff(
        &self,
        mut affs: Vec<(u16, usize, usize)>,
        sig_i: Vec<usize>,
        p: usize,
    ) -> Vec<usize> {
        // sort affinities so the last element is the best
        affs.sort_by(cmp_affinities);
        debug!("Sort is finished");
        debug!("len_i = {},N turn = {}", self.len_i, self.len_i / p);
        assert!(self.len_i * self.cur_band == sig_i.len());
        assert!(self.len_i % p == 0);

        let mut singles = Singles::new(self.len_i);
        let mut new_sig_i = sig_i.clone();

        let mut idx = 0;
        let n_turns = self.len_i / p;
        // each turn we select one group of p rows (= p-uplet)
        for _turn in 0..n_turns {
            // get best and remove the already coupled rows
            let (_, mut i1, mut i2) = affs.pop().unwrap();
            while !(singles.contains(i1) && singles.contains(i2)) {
                let tmp = affs.pop().unwrap();
                i1 = tmp.1;
                i2 = tmp.2;
            }

            // here i1 and i2 are single and will be coupled
            // update singles since they are coupled
            singles.remove(i1);
            singles.remove(i2);

            // append to the couple until the p-uplet is completed
            let mut p_uplets = vec![i1, i2];
            while p_uplets.len() != p {
                // get best from the couples involving one known row among the p-uplet
                let (idx, i3) = affs
                    .iter()
                    .enumerate()
                    .rev()
                    .find_map(|(idx, (_, i1b, i2b))| {
                        if p_uplets.contains(i1b) && singles.contains(*i2b) {
                            Some((idx, *i2b))
                        } else if p_uplets.contains(i2b) && singles.contains(*i1b) {
                            Some((idx, *i1b))
                        } else {
                            None
                        }
                    })
                    .unwrap();

                // update singles since i3 is now coupled
                singles.remove(i3);
                affs.remove(idx);
                p_uplets.push(i3);
            }

            // p-uplet completed so now we update the permutation
            p_uplets.sort();
            for i in p_uplets {
                let band_i = i * self.cur_band;
                for &old_i in sig_i.iter().skip(band_i).take(self.cur_band) {
                    new_sig_i[idx] = old_i;
                    idx += 1;
                }
            }
        }
        debug_assert!(singles.is_empty());
        new_sig_i
    }
}

/// Utility for the primes method that need to get all prime factors of the band len/height.
fn prime_factors_of(mut n: usize) -> Vec<usize> {
    let mut res: Vec<usize> = vec![];
    let mut prime = 2;

    while n > 1 {
        if n % prime == 0 {
            n /= prime;
            res.push(prime);
        } else {
            prime += 1;
        }
    }
    res
}

/// Utility to sort affinity in increasing order but indices in decreasing order
fn cmp_affinities(a1: &(u16, usize, usize), a2: &(u16, usize, usize)) -> Ordering {
    match a1.0.cmp(&a2.0) {
        Ordering::Equal => match a2.1.cmp(&a1.1) {
            Ordering::Equal => a2.2.cmp(&a1.2),
            order => order,
        },
        order => order,
    }
}

#[cfg(test)]
mod test_permutation {
    use super::*;

    #[test]
    fn test_prime_factors() {
        let empty: Vec<usize> = vec![];
        assert_eq!(prime_factors_of(0), empty);
        assert_eq!(prime_factors_of(1), empty);
        assert_eq!(prime_factors_of(2), vec![2]);
        assert_eq!(prime_factors_of(3), vec![3]);
        assert_eq!(prime_factors_of(4), vec![2, 2]);
        assert_eq!(prime_factors_of(10), vec![2, 5]);
        assert_eq!(prime_factors_of(30), vec![2, 3, 5]);
    }
    #[test]
    fn test_singles() {
        let mut singles = Singles::new(10);
        assert!(singles.contains(2));
        singles.remove(2);
        assert!(!singles.contains(2));
        assert_eq!(singles.set[0].count_zeros(), 1);

        assert!(singles.contains(3));
        singles.remove(3);
        assert!(!singles.contains(3));
        assert_eq!(singles.set[0].count_zeros(), 2);
        // idempotent
        singles.remove(2);
        assert_eq!(singles.set[0].count_zeros(), 2);
    }
}
