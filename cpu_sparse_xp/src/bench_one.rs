use crate::exp::Exp;
use crate::mkl_bindings::MklSparseFormats;
use crate::mkl_dense::MklDense;
use crate::mkl_sparse::MklSparseImpl;
use crate::model_best_kern::{find_best_kernel, DEFAULT_HEATMAP};
use crate::permutation::ReorderingAlgos;
use crate::utils::*;
use crate::CONFIG;
use crate::F;
use clap::Args;
use gcd::Gcd;
use log::debug;
use log::info;
use operator_benchmarks::*;
use serde::Serialize;
use spmmgen::*;
use std::collections::HashMap;
use std::mem::size_of;
use std::path::PathBuf;

/// Meta data to add in the json result of the benchmark.
/// This should be modify with caution since some python scripts may assume some fields to be
/// present or to have a specific type.
/// So rename fields or deleting fields or changing type might break some scripts.
#[derive(Serialize)]
pub struct BenchOneMetaData {
    len_i: usize,
    len_j: usize,
    len_k: usize,
    ukernel: MicroKernel,
    kernel_perm_i: usize,
    kernel_perm_j: usize,
    nnz: usize,
    nnz_bands: usize,
    nnz_bands_auto: usize,
    nnz_bands_permuted: HashMap<ReorderingAlgos, usize>,
    nnz_blocks: usize,
    tile_j: Option<usize>,
    blocksize_k: usize,
    bsr_square: usize,
    matrix: String,
}

/// Specific arguments and options for using the bench-one subcommand.
/// Some can also be changed from code and not from command line.
#[derive(Args)]
pub struct ArgsBenchOne {
    /// The number of columns of the 2 denses matrices B and C in C=AxB.
    #[arg(long, default_value_t = 128)]
    len_j: usize,

    /// The unroll factor on i on the inner most loop.
    /// It is useful on blocks and bands only.
    #[arg(long, default_value_t = 4)]
    kernel_i: usize,

    /// The unroll factor on j on the inner most loop.
    #[arg(long, default_value_t = 4)]
    kernel_j: usize,

    /// Same as kernel_i but for permuted band.
    #[arg(long, default_value_t = 6)]
    kernel_ip: usize,

    /// Same as kernel_j but for permuted band.
    #[arg(long, default_value_t = 2)]
    kernel_jp: usize,

    /// Choose the vector size to use.
    #[arg(long, default_value_t=VectorInstructions::Avx2)]
    avx: VectorInstructions,

    /// Enforce the size in blocks implementation. By default (option not set).
    /// it will be choosen by taking the biggest number dividing len_k between 1 and 32.
    #[arg(long = "block-k")]
    block_k: Option<usize>,

    /// Optional Parametter to force tiling of a factor n on the j dimension.
    /// Example: if ntilej=3 and kernel_j=2 inner j loop of 3*2 vectors ,outer loop with step (3*2*VECSIZE).
    #[arg(long = "ntilej")]
    tiledivj: Option<usize>,

    /// list of the reorder_algos to try
    #[arg(long, value_delimiter = ' ', default_value = "primes primes-gorder")]
    reorder_algos: Vec<ReorderingAlgos>,

    /// Path to the matrix market file representing the sparse matrix as input for SpMM.
    mtx: PathBuf,

    // difference with default_value_t is that it is the preprocessed input
    // here the default_value_t will not compile since Vec<T> doesn't implement Display
    #[arg(
        value_delimiter = ' ',
        default_value = "bands permuted-bands mkl-csr mkl-bsr mkl-coo"
    )]
    implementations: Vec<ImplemKind>,
}

impl ArgsBenchOne {
    /// Create a default [ArgsBenchOne].
    pub fn new<T>(mtx: T) -> Self
    where
        PathBuf: From<T>,
    {
        ArgsBenchOne {
            mtx: PathBuf::from(mtx),
            implementations: vec![
                ImplemKind::PermutedBands,
                ImplemKind::Bands,
                ImplemKind::MklCsr,
                ImplemKind::MklBsr,
                ImplemKind::MklCoo,
            ],
            reorder_algos: vec![ReorderingAlgos::Primes, ReorderingAlgos::PrimesGorder],
            kernel_i: 4,
            kernel_j: 4,
            kernel_ip: 6,
            kernel_jp: 2,
            avx: VectorInstructions::Avx2,
            len_j: 128,
            block_k: None,
            tiledivj: None,
        }
    }

    /// Change the kernel_i and j and avx to match the given [MicroKernel].
    pub fn with_kernel(mut self, kernel: MicroKernel) -> Self {
        self.kernel_i = kernel.i;
        self.kernel_j = kernel.j;
        self.avx = kernel.avx;
        self
    }

    /// Change the kernel_ip and jp.
    pub fn with_kernel_permut(mut self, i: usize, j: usize) -> Self {
        self.kernel_ip = i;
        self.kernel_jp = j;
        self
    }

    /// Overide the default list of implementations to benchmark.
    pub fn with_implementations(mut self, impls: &[ImplemKind]) -> Self {
        self.implementations = impls.to_vec();
        self
    }

    /// Overide the default list of [ReorderingAlgos] to benchmark when using [PermutedBands].
    pub fn with_reordering_algos(mut self, algos: &[ReorderingAlgos]) -> Self {
        self.reorder_algos = algos.to_vec();
        self
    }
}

impl ArgsBenchOne {
    /// Benchmark according to the current fields content and to the given compiler.
    /// This is the function that has been changing the most and that contains the most code for
    /// handling all the evolving implementations. It is the messiest part of these project.
    /// At a high level this function:
    /// - load the mtx file to a [CooMatrix] using [matrix_market_rs].
    /// - initialize a [Benchmark]
    /// - for each `implementations`:
    ///     - prepare inputs (matrices B, C and maybe some permutation vector or custom sparse format).
    ///     - execute and measure with [Benchmark::run].
    /// - return the [BenchResult] with the corresponding meta data [BenchOneMetaData].
    pub fn benchmark(self, compiler: String) -> BenchResult<BenchOneMetaData> {
        // prepare data buffers

        info!("Benchmarking {:?}", self.mtx);
        let coo = CooMatrix::from_mtx_file(&self.mtx);
        let nnz = coo.triplets.len();
        let kernel = MicroKernel {
            i: self.kernel_i,
            j: self.kernel_j,
            avx: self.avx,
        };
        debug!("Created coo_args");
        let len_k = coo.shape[1];
        let len_j = self.len_j;
        let vecsize = kernel.avx.vector_size::<F>();
        let (bi, bj) = (kernel.i, kernel.j);
        let bk = self
            .block_k
            .unwrap_or((1..=32).rev().find(|bk| len_k % bk == 0).unwrap());

        debug!("Creating buffers");
        let [a, b, mut c] = padded_dense_buffers(bi, bj, &coo, len_j, vecsize); // Prepare data for our implementations
                                                                                // prepare benchmark
        debug!("Buffers created");
        let mut bench = Benchmark::from_toml_file(CONFIG);
        let counters = bench.counters_names();
        let machine = Machine {
            vector_size: (vecsize * size_of::<F>()) as u64,
            ..Default::default()
        };
        let reporters: Vec<Box<dyn MetricReporter>> = vec![
            Box::new(PeakPerfReporter::new(
                machine,
                nnz * len_j,
                size_of::<F>(),
                1,
                &counters,
            )),
            Box::new(GflopsReporter::new(&counters)),
            Box::new(UsefulGFlops::new(len_j * nnz)),
        ];

        bench = bench.with_metrics_reporters(reporters);
        let generator = CodeGenerator::new().with_compiler(compiler);

        // run our implementations
        let mut nnz_bands = nnz;
        let mut nnz_bands_auto = nnz;
        let mut nnz_bands_permuted: HashMap<ReorderingAlgos, usize> = HashMap::new();
        let mut nnz_blocks = nnz;
        for implem in self.implementations.into_iter() {
            debug!("Creating {:?}", implem);
            match implem {
                ImplemKind::PermutedBands => {
                    // we dont use the same kernel for Bands and PermutedBands
                    let kernel = MicroKernel {
                        i: self.kernel_ip,
                        j: self.kernel_jp,
                        avx: self.avx,
                    };
                    // THIS IS NECESSARY since padding might be different
                    // for permuted and non permuted case
                    let [a, b, mut c] =
                        padded_dense_buffers(kernel.i, kernel.j, &coo, len_j, vecsize);
                    debug!(
                        "permutation band bi = {}, shape = ({},{})",
                        self.kernel_ip, a.pad_nrows, a.pad_ncols
                    );
                    for &algo in self.reorder_algos.iter() {
                        let perm = get_perm_cache_or_algo(&self.mtx, &coo, kernel.i, algo);
                        let perm_bands = PermutedBands::new(&a, kernel.i, self.tiledivj, &perm);
                        nnz_bands_permuted.insert(algo, perm_bands.band.values.len());
                        let exp = Exp::new(&generator, perm_bands, kernel, &b, &mut c)
                            .with_name(format!("BandsPerm{algo:?}"));
                        bench.run(exp);
                    }
                }
                ImplemKind::AutoSizedBands => {
                    let kernel = find_best_kernel(self.avx, &coo, DEFAULT_HEATMAP);
                    let [a, b, mut c] =
                        padded_dense_buffers(kernel.i, kernel.j, &coo, len_j, vecsize);
                    let bands = Bands::new(&a, kernel.i, self.tiledivj);
                    nnz_bands_auto = bands.values.len();
                    let exp = Exp::new(&generator, bands, kernel, &b, &mut c)
                        .with_name("BandsAutoSized".to_owned());
                    bench.run(exp);
                }
                ImplemKind::Bands => {
                    debug!("bi = {bi}, shape = ({},{})", a.pad_nrows, a.pad_ncols);
                    let bands = Bands::new(&a, bi, self.tiledivj);
                    nnz_bands = bands.values.len();
                    bench.run(Exp::new(&generator, bands, kernel, &b, &mut c));
                }
                ImplemKind::Blocks => {
                    let block = Blocks::new(&a, bi, bk, self.tiledivj);
                    nnz_blocks = block.values.len();
                    bench.run(Exp::new(&generator, block, kernel, &b, &mut c));
                }
                ImplemKind::Bourrin => {
                    let bourrin = Bourrin::from_sparse(&coo.triplets);
                    bench.run(Exp::new(&generator, bourrin, kernel, &b, &mut c));
                }

                ImplemKind::Coo => {
                    let spec = Coo::new(&coo.triplets, self.tiledivj);
                    bench.run(Exp::new(&generator, spec, kernel, &b, &mut c));
                }
                ImplemKind::Csr => {
                    let spec = Csr::new(&coo.triplets, coo.shape[0], self.tiledivj);
                    bench.run(Exp::new(&generator, spec, kernel, &b, &mut c));
                }
                ImplemKind::MklCsr => {
                    bench.run(MklSparseImpl::new(MklSparseFormats::Csr, &coo, &b, &mut c));
                }
                ImplemKind::MklCoo => {
                    bench.run(MklSparseImpl::new(MklSparseFormats::Coo, &coo, &b, &mut c));
                }
                ImplemKind::MklBsr => {
                    bench.run(MklSparseImpl::new(
                        MklSparseFormats::Bsr(bi.gcd(bk) as i64),
                        &coo,
                        &b,
                        &mut c,
                    ));
                }
                ImplemKind::MklDense => {
                    bench.run(MklDense::new(&coo, len_j as i64, &a, &b, &mut c));
                }
            }
        }

        let meta = BenchOneMetaData {
            len_i: coo.shape[0],
            len_k: coo.shape[1],
            kernel_perm_i: self.kernel_ip,
            kernel_perm_j: self.kernel_jp,
            len_j,
            ukernel: kernel,
            nnz,
            nnz_bands,
            nnz_bands_auto,
            nnz_bands_permuted,
            nnz_blocks,
            tile_j: self.tiledivj,
            blocksize_k: bk,
            bsr_square: bi.gcd(bk),
            matrix: self.mtx.to_str().unwrap().to_owned(),
        };
        bench.result(meta)
    }
}
