use crate::utils::CooMatrix;
use crate::F;
use clap::Args;
use ittapi::StringHandle;
use ittapi::{Domain, Task};
use spmmgen::*;
use std::path::PathBuf;
use std::time::SystemTime;

#[derive(Args)]
pub struct ArgsDebugPerf {
    // implem: ImplemKind,
    #[arg(long = "J", default_value_t = 128)]
    len_j: usize,

    #[arg(long = "repeat", default_value_t = 100)]
    repeat: usize,
}

#[link(name = "debug_perf")]
extern "C" {
    fn debug_matmul_by_bourrin(J: i32, B: *const f32, C: *mut f32);
}

impl ArgsDebugPerf {
    pub fn run(self) {
        let path = PathBuf::from("matrices/ACTIVSg10K.mtx");
        println!("Analyzing {:?}", path);

        let coo_args = CooMatrix::from_mtx_file(path);
        let len_j = self.len_j;
        let bi = 6;
        let bj = 4;

        let ukern = MicroKernel {
            i: bi,
            j: bj,
            avx: VectorInstructions::default(),
        };
        let vecsize = ukern.avx.vector_size::<F>();
        let bj = bj * vecsize;
        let len_i = coo_args.shape[0];
        let len_k = coo_args.shape[1];
        let pad_i = (len_i + bi - 1) / bi * bi;
        let pad_j = (len_j + bj - 1) / bj * bj;
        println!("Allocating Buffers for Dense matrices.");
        let b: DenseMatrix<F> = DenseMatrix::filled_padding(len_k, len_j, len_k, pad_j);
        let mut c: DenseMatrix<F> = DenseMatrix::filled_padding(len_i, len_j, pad_i, pad_j);

        // run and notify vtune
        // example from https://docs.rs/ittapi/latest/ittapi/struct.Task.html

        let start = SystemTime::now();
        let domain = Domain::new("domain");
        let name = StringHandle::new("TaskSpmm");

        println!("Executing {} times", self.repeat);
        unsafe {
            let task = Task::begin(&domain, name);
            for _ in 0..self.repeat {
                debug_matmul_by_bourrin(len_j as i32, b.buf.as_ptr(), c.buf.as_mut_ptr());
            }

            task.end();
        }
        println!("Task take {:?}", start.elapsed().unwrap());
        println!("c = {:?}", &c.buf[0..10]);
    }
}
