use crate::F;
use libloading::Library;
use operator_benchmarks::Computation;
use spmmgen::MicroKernel;
use spmmgen::{CodeGenerator, DenseMatrix, TemplateSpmm};

pub struct Exp<T> {
    name: String,
    spec: T,
    lib: Library,
    j: i32,
    b: *const F,
    c: *mut F,
}

impl<T: TemplateSpmm<F>> Exp<T> {
    pub fn new(
        generator: &CodeGenerator,
        spec: T,
        ukern: MicroKernel,
        dense_b: &DenseMatrix<F>,
        dense_c: &mut DenseMatrix<F>,
    ) -> Self {
        Exp {
            name: format!("{spec}"),
            lib: generator.gen_compile_load(&spec, ukern).unwrap(),
            spec,
            j: dense_c.pad_ncols as i32,
            b: dense_b.buf.as_ptr(),
            c: dense_c.buf.as_mut_ptr(),
        }
    }

    pub fn with_name(mut self, name: String) -> Self {
        self.name = name;
        self
    }
}

impl<T: TemplateSpmm<F>> Computation for Exp<T> {
    fn execute(&mut self) {
        unsafe { self.spec.execute_gen_fn(&self.lib, self.j, self.b, self.c) }
    }

    fn name(&self) -> &str {
        self.name.as_str()
    }
}
