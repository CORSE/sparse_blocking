use clap::Args;
use log::info;
use std::{
    fs::read_to_string,
    path::{Path, PathBuf},
};

use spmmgen::{Bands, MicroKernel, VectorInstructions};

use crate::utils::{padded_dense_a, CooMatrix};

pub const DEFAULT_HEATMAP: &str = "results/pinocchio_heatmap.csv";
#[derive(Args)]
pub struct ArgsBestKernel {
    matrix: PathBuf,

    #[arg(long, default_value_t = VectorInstructions::Avx512)]
    avx: VectorInstructions,

    #[arg(long = "heatmap", default_value = DEFAULT_HEATMAP)]
    heatmap_path: PathBuf,
}
fn get_heatmap<T: AsRef<Path>>(heatmap_path: T, n: usize) -> Vec<Vec<f64>> {
    let s = read_to_string(heatmap_path).unwrap();
    let mut heatmap: Vec<Vec<f64>> = vec![vec![0.0; n]; n];
    for line in s.trim_end().lines().skip(1) {
        // i,j,k,res
        let mut it = line.split(',');
        let i: usize = it.next().unwrap().parse().unwrap();
        let j: usize = it.next().unwrap().parse().unwrap();
        it.next().unwrap();
        let res: f64 = it.next().unwrap().parse().unwrap();
        heatmap[i][j] = res;
    }
    heatmap
}

pub fn find_best_kernel<T: AsRef<Path>>(
    avx: VectorInstructions,
    coo: &CooMatrix,
    heatmap_path: T,
) -> MicroKernel {
    let n = match avx {
        VectorInstructions::Avx2 => 16,
        VectorInstructions::Avx512 => 32,
    };
    let nnz = coo.triplets.len() as f64;
    let heatmap = get_heatmap(heatmap_path, n);
    let (zf, perf, i, j) = (1..=((n - 1) / 2) - 1)
        .flat_map(|i| {
            let dense = padded_dense_a(i, coo);
            let band = Bands::new(&dense, i, None);
            let zerofilling = (band.values.len() as f64) / nnz;
            (2..=((n - 1) / (i + 1))).map({
                let heatmap = &heatmap;
                move |j| (zerofilling, heatmap[i][j], i, j)
            })
        })
        .min_by_key(|(zf, perf, _i, _j)| {
            // debug!("i={_i} j={_j} zf={zf:.2} perf={perf:.2}");
            (zf / perf * 1000.0) as i64
        })
        .unwrap();

    info!("Best Kernel {i}x{j} (zf={zf:.2}, perf={perf:.2})");
    MicroKernel { i, j, avx }
}
impl ArgsBestKernel {
    pub fn find_best_kernel(self) -> MicroKernel {
        let coo = CooMatrix::from_mtx_file(&self.matrix);
        info!("Matrix {:?}", self.matrix);
        find_best_kernel(self.avx, &coo, self.heatmap_path)
    }
}
