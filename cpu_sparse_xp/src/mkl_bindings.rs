use std::fmt::{Debug, Display};

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum Layout {
    RowMajor = 101,
    #[allow(dead_code)]
    ColumMajor = 102,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum Transpose {
    No = 111,
    #[allow(dead_code)]
    Yes = 112,
    #[allow(dead_code)]
    Conj = 113,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum SparseTranspose {
    No = 10,
    #[allow(dead_code)]
    Yes = 11,
    #[allow(dead_code)]
    Conj = 12,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub struct MatrixDescr {
    pub r#type: MatrixType, // r# is here to allow type (a keyword) to be a field name
    pub mode: FillMode,     // in order to have the same API than MKL it is necessary
    pub diag: DiagType,
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum MatrixType {
    General = 20,
    #[allow(dead_code)]
    Symmetric = 21,
    #[allow(dead_code)]
    Hermitian = 22,
    #[allow(dead_code)]
    Triangular = 23,
    #[allow(dead_code)]
    Diagonal = 24,
    #[allow(dead_code)]
    BlockTriangular = 25,
    #[allow(dead_code)]
    BlockDiagonal = 26,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum FillMode {
    #[allow(dead_code)]
    LowerTriangular = 40,
    #[allow(dead_code)]
    UpperTriangular = 41,
    Full = 42,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum DiagType {
    NonUnit = 50,
    #[allow(dead_code)]
    Unit = 51,
}

#[repr(C)]
#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum SparseStatus {
    Sucess = 0,
    #[allow(dead_code)]
    NotInitialized = 1,
    #[allow(dead_code)]
    AllocFailed = 2,
    #[allow(dead_code)]
    InvalidValue = 3,
    #[allow(dead_code)]
    ExecutionFailed = 4,
    #[allow(dead_code)]
    InternalError = 5,
    NotSupported = 6,
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
pub enum BaseIndex {
    Zero = 0,
    #[allow(dead_code)]
    One = 1,
}

// Opaque struct sparse_matrix_t in MKL
#[repr(C)]
pub struct SparseMatrix {
    _data: [u8; 0],
    _marker: core::marker::PhantomData<(*mut u8, core::marker::PhantomPinned)>,
}

extern "C" {
    pub fn cblas_sgemm(
        layout: Layout,
        transa: Transpose,
        transb: Transpose,
        m: i64,
        n: i64,
        k: i64,
        alpha: f32,
        a: *const f32,
        lda: i64,
        b: *const f32,
        ldb: i64,
        beta: f32,
        c: *mut f32,
        ldc: i64,
    );

    #[allow(dead_code)]
    pub fn cblas_dgemm(
        layout: Layout,
        transa: Transpose,
        transb: Transpose,
        m: i64,
        n: i64,
        k: i64,
        alpha: f64,
        a: *const f64,
        lda: i64,
        b: *const f64,
        ldb: i64,
        beta: f64,
        c: *mut f64,
        ldc: i64,
    );

    pub fn mkl_sparse_s_mm(
        operation: SparseTranspose,
        alpha: f32,
        A: *const SparseMatrix,
        descr: MatrixDescr,
        layout: Layout,
        B: *const f32,
        columns: i64,
        ldb: i64,
        beta: f32,
        C: *mut f32,
        ldc: i64,
    ) -> SparseStatus;

    #[allow(dead_code)]
    pub fn mkl_sparse_d_mm(
        operation: SparseTranspose,
        alpha: f64,
        A: *const SparseMatrix,
        descr: MatrixDescr,
        layout: Layout,
        B: *const f64,
        columns: i64,
        ldb: i64,
        beta: f64,
        C: *mut f64,
        ldc: i64,
    ) -> SparseStatus;

    // inspector phase functions of the inspector executor routines
    pub fn mkl_sparse_set_mm_hint(
        A: *const SparseMatrix,
        operation: SparseTranspose,
        descr: MatrixDescr,
        layout: Layout,
        dense_matrix_size: i64,
        expected_calls: i64,
    ) -> SparseStatus;

    pub fn mkl_sparse_optimize(A: *mut SparseMatrix) -> SparseStatus;
    pub fn mkl_sparse_convert_csr(
        source: *const SparseMatrix,
        operation: SparseTranspose,
        dest: *mut *mut SparseMatrix,
    ) -> SparseStatus;
    pub fn mkl_sparse_convert_bsr(
        source: *const SparseMatrix,
        blocsize: i64,
        bloclayout: Layout,
        operation: SparseTranspose,
        dest: *mut *mut SparseMatrix,
    ) -> SparseStatus;

    pub fn mkl_sparse_s_create_coo(
        A: *mut *mut SparseMatrix,
        indexing: BaseIndex,
        rows: i64,
        cols: i64,
        nnz: i64,
        row_indx: *mut i64,
        col_indx: *mut i64,
        values: *mut f32,
    ) -> SparseStatus;

    #[allow(dead_code)]
    pub fn mkl_sparse_d_create_coo(
        A: *mut *mut SparseMatrix,
        indexing: BaseIndex,
        rows: i64,
        cols: i64,
        nnz: i64,
        row_indx: *mut i64,
        col_indx: *mut i64,
        values: *mut f64,
    ) -> SparseStatus;
    pub fn mkl_sparse_destroy(A: *mut SparseMatrix) -> SparseStatus;
}

#[derive(Debug, Clone, Copy)]
pub enum MklSparseFormats {
    Coo,
    Csr,
    Bsr(i64),
}

impl Display for MklSparseFormats {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MklSparseFormats::Bsr(_) => write!(f, "MKL Bsr"),
            _ => write!(f, "MKL {self:?}"),
        }
    }
}

#[cfg(test)]
mod test_mkl_bindings {
    use super::*;
    use std::iter::repeat;

    fn naive_matmul(m: usize, n: usize, l: usize, a: Vec<f32>, b: Vec<f32>) -> Vec<f32> {
        let mut c: Vec<f32> = repeat(0.0).take(m * n).collect();
        for i in 0..m {
            for k in 0..l {
                for j in 0..n {
                    c[i * n + j] += a[i * l + k] * b[k * l + j];
                }
            }
        }
        c
    }

    const EPSILON: f32 = 0.01;
    fn epsilon_eq(v1: Vec<f32>, v2: Vec<f32>) {
        for (a, b) in v1.into_iter().zip(v2.into_iter()) {
            assert!((a - b).abs() < EPSILON);
        }
    }

    #[test]
    fn test_dense_matmul() {
        let (m, n, k) = (5, 7, 6);
        let a: Vec<f32> = (0..(m * k)).map(|i| 0.1 * i as f32).collect();
        let b: Vec<f32> = (0..(k * n)).map(|_| 0.2).collect();
        let mut c: Vec<f32> = (0..(m * n)).map(|_| 0.0).collect();
        unsafe {
            cblas_sgemm(
                Layout::RowMajor,
                Transpose::No,
                Transpose::No,
                m,
                n,
                k,
                1.0,
                a.as_ptr(),
                k,
                b.as_ptr(),
                n,
                0.0,
                c.as_mut_ptr(),
                n,
            );
        }
        let expected = naive_matmul(m as usize, n as usize, k as usize, a, b);
        epsilon_eq(c, expected);
    }
}
