use crate::permutation::ReorderingAlgos;
use crate::F;

use clap::ValueEnum;
use log::info;
use matrix_market_rs::{MtxData, SymInfo};
use serde::Serialize;
use spmmgen::DenseMatrix;
use std::fmt::Debug;
use std::fmt::Display;
use std::fs::create_dir_all;
use std::fs::read_to_string;
use std::fs::File;
use std::io::Write;
use std::path::Path;
use std::path::PathBuf;

/// The main utility struct that is build from mtx file and can be easily converted to [DenseMatrix].
/// Also the triplets contain the symmetric data if the mtx file start with the "symmetric" banner.
#[derive(Clone, PartialEq)]
pub struct CooMatrix {
    /// length of both axis i and k
    pub shape: [usize; 2],

    /// For each nonzero (i, k, value).
    pub triplets: Vec<(usize, usize, F)>,
}

fn extract_sparse_mtx<P: AsRef<Path> + Debug>(
    mtx: P,
) -> ([usize; 2], Vec<[usize; 2]>, Vec<F>, SymInfo) {
    let path = format!("{mtx:?}");
    let mtx_a: MtxData<F, 2> = MtxData::from_file(mtx).unwrap();
    if let MtxData::Sparse(shape, ind, nnz, s) = mtx_a {
        (shape, ind, nnz, s)
    } else {
        panic!("{path} was expected to contains sparse matrix not dense one.");
    }
}

impl CooMatrix {
    /// Helper to easily permute rows for permutation algoritm in several steps such as primes +
    /// gorder.
    pub fn permuted_rows(&self, perm: &[usize]) -> Self {
        let perm_inv = inverse_perm_vec(perm);
        let mut gorder_coo = self.clone();
        gorder_coo.triplets = gorder_coo
            .triplets
            .into_iter()
            .map(|(i, k, v)| (perm_inv[i], k, v))
            .collect();
        gorder_coo
    }

    /// Read mtx file and add the symmetric values in case the matrix is symmetric.
    pub fn from_mtx_file<P: AsRef<Path> + Debug>(mtx: P) -> Self {
        let (shape, indices, nonzeros, sym) = extract_sparse_mtx(mtx);
        let mut triplets: Vec<(usize, usize, F)> = Vec::with_capacity(nonzeros.len());
        for ([i, k], val) in indices.into_iter().zip(nonzeros) {
            triplets.push((i, k, val));
            if sym == SymInfo::Symmetric && i != k {
                triplets.push((k, i, val));
            }
        }
        triplets.sort_by(|a, b| (a.0, a.1).cmp(&(b.0, b.1)));
        CooMatrix { shape, triplets }
    }
}

/// Useful function to initialize the 3 dense buffers for A, B and C.
pub fn padded_dense_buffers(
    bi: usize,
    bj: usize,
    coo: &CooMatrix,
    len_j: usize,
    vecsize: usize,
) -> [DenseMatrix<F>; 3] {
    let [len_i, len_k] = coo.shape;
    let pad_i = len_i.next_multiple_of(bi);
    let pad_j = len_j.next_multiple_of(bj * vecsize);
    let dense_a = DenseMatrix::from_coo_padded(coo.shape, &coo.triplets, pad_i, len_k);
    let b: DenseMatrix<F> = DenseMatrix::filled_padding(len_k, len_j, len_k, pad_j);
    let c: DenseMatrix<F> = DenseMatrix::zeros_padded(len_i, len_j, pad_i, pad_j);
    [dense_a, b, c]
}

/// Useful function to initialize only 1 dense buffer A.
pub fn padded_dense_a(bi: usize, coo: &CooMatrix) -> DenseMatrix<F> {
    let [len_i, len_k] = coo.shape;
    let pad_i = len_i.next_multiple_of(bi);
    DenseMatrix::from_coo_padded(coo.shape, &coo.triplets, pad_i, len_k)
}

/// Get the number of vector registers available.
pub fn count_ymm_registers() -> usize {
    if is_x86_feature_detected!("avx512vl") {
        32
    } else if is_x86_feature_detected!("avx2") {
        16
    } else {
        0
    }
}

/// Enum to choose the implementation to run from command line interface.
#[derive(Debug, Copy, Clone, ValueEnum, PartialEq, Eq)]
pub enum ImplemKind {
    Blocks,
    Bands,
    PermutedBands,
    AutoSizedBands,
    Bourrin,
    Coo,
    Csr,
    MklCsr,
    MklCoo,
    MklBsr,
    MklDense,
}

impl Display for ImplemKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", format!("{self:?}").to_lowercase())
    }
}

/// Useful for saving a [BenchResult](operator_benchmarks::BenchResult) to a given path.
pub fn dump_to_json<S: Serialize, P: AsRef<Path> + Debug>(serializable: &S, path: P) {
    info!("Writing {path:?}");
    let res_str = serde_json::to_string_pretty(serializable).unwrap();
    let mut f = File::options().write(true).create(true).open(path).unwrap();
    f.write_all(res_str.as_bytes()).unwrap();
}

/// Create a csv file with the permutation if it doesnt exists
/// or read the permutation from the file if it exists
pub fn get_perm_cache_or_algo(
    matname: &PathBuf,
    coo: &CooMatrix,
    band_len: usize,
    algo: ReorderingAlgos,
) -> Vec<usize> {
    let path_dir_permutation = PathBuf::from("permutations").join(matname);
    if !path_dir_permutation.exists() {
        info!("Creating dir {path_dir_permutation:?}");
        create_dir_all(&path_dir_permutation).unwrap();
    }

    let new_perm_file = match algo {
        ReorderingAlgos::Gorder => format!("{algo:?}"),
        _ => format!("{algo:?}_i{band_len}.csv"),
    };

    let new_perm_file = path_dir_permutation.join(new_perm_file);
    if !new_perm_file.exists() {
        // we dont have the permutation in cache so we compute it
        let perm_vec = algo.run(coo, band_len as u8, matname);
        let perm_vec_str: Vec<String> = perm_vec.iter().map(usize::to_string).collect();
        let perm_vec_str = perm_vec_str.join(",");
        info!("Creating {new_perm_file:?}");
        let mut f = File::create(new_perm_file).unwrap();
        f.write_all(perm_vec_str.as_bytes()).unwrap();
        perm_vec
    } else {
        // read permutation from file instead of computing it
        info!("Reading {new_perm_file:?}");
        let perm_vec_str = read_to_string(new_perm_file).unwrap();
        perm_vec_str
            .trim_end()
            .split(',')
            .map(|x| x.parse().unwrap())
            .collect()
    }
}

/// Transform a permutation vector `perm` such as `perm[new_i] = old_i` in the reversed way
/// `perm_inv[old_i] = new_i`.
pub fn inverse_perm_vec(perm: &[usize]) -> Vec<usize> {
    let n = perm.len();
    let mut perm_inv = vec![0; n];
    for (new_i, &old_i) in perm.iter().enumerate() {
        perm_inv[old_i] = new_i;
    }
    perm_inv
}

#[test]
fn test_inv_perm_vec() {
    let id: Vec<usize> = (0..10).collect();
    let id_inv = inverse_perm_vec(&id);
    assert_eq!(id, id_inv);

    let shift2: Vec<usize> = (0..10).map(|i| (i + 2) % 10).collect();
    let shift2_inv: Vec<usize> = (0..10).map(|i| (10 + i - 2) % 10).collect();
    assert_eq!(shift2_inv, inverse_perm_vec(&shift2));
}
