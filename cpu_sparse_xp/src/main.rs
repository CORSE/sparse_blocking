mod bench_one;
mod bench_suit;
mod bruteforce;
mod debug_perf;
mod exp;
mod mkl_bindings;
mod mkl_dense;
mod mkl_sparse;
mod model_best_kern;
mod permutation;
mod utils;

use model_best_kern::ArgsBestKernel;
use permutation::ArgsTestPermut;
use std::{fs::File, path::PathBuf};

use bench_one::ArgsBenchOne;
use bench_suit::ArgsBenchSuit;
use bruteforce::ArgsBruteForce;
use clap::{Parser, Subcommand};
use debug_perf::ArgsDebugPerf;
use simplelog::{ColorChoice, Config, LevelFilter, TermLogger, TerminalMode};

use crate::utils::dump_to_json;

/// Floating type to use for generated code
type F = f32;

/// Default path for the benchmark configuration file
const CONFIG: &str = "config.toml";

/// Basic flags that are common across all subcommands.
#[derive(Parser)]
#[command(author, version, about, long_about = None)]
pub struct CliArgs {
    #[arg(long, default_value_t = String::from("gcc"))]
    compiler: String,

    #[arg(long = "debug", default_value_t = false)]
    debug_log: bool,

    #[arg(long)]
    output: Option<PathBuf>,

    #[command(subcommand)]
    command: Commands,
}

/// Subcommand for the command line interface.
#[derive(Subcommand)]
enum Commands {
    /// Process one matrix with various implementations
    BenchOne(ArgsBenchOne),

    /// Do the same for a collection of matrices in the same directory
    BenchSuit(ArgsBenchSuit),

    /// Bruteforce experiment by trying various sizes of micro kernels and matrices
    Bruteforce(ArgsBruteForce),

    /// Execute only 1 execution with vtune to debug perf.
    DebugPerf(ArgsDebugPerf),

    /// Small test command for debugging the permutation algorithm.
    TestPermutation(ArgsTestPermut),

    /// Try the model of prediction for best kernel of the Band implementation.
    BestKernel(ArgsBestKernel),
}

fn main() {
    let args = CliArgs::parse();
    let log_level = if args.debug_log {
        LevelFilter::Debug
    } else {
        LevelFilter::Info
    };
    TermLogger::init(
        log_level,
        Config::default(),
        TerminalMode::Mixed,
        ColorChoice::Auto,
    )
    .unwrap();
    use Commands::*;
    match args.command {
        DebugPerf(debug) => {
            debug.run();
        }
        Bruteforce(ranges) => {
            let res = ranges.bruteforce(args.compiler);
            res.show_summary();
            if let Some(path) = args.output {
                dump_to_json(&res, path);
            }
        }
        BenchOne(onemtx) => {
            let res = onemtx.benchmark(args.compiler);
            res.show_summary();
            if let Some(path) = args.output {
                dump_to_json(&res, path);
            }
        }
        BenchSuit(b) => b.benchmark_with_cache(args.compiler),
        BestKernel(b) => {
            let kernel = b.find_best_kernel();
            println!("Best kernel found {kernel}");
        }

        TestPermutation(ArgsTestPermut {
            mat,
            band_len,
            algo,
        }) => {
            use utils::CooMatrix;
            let coo = CooMatrix::from_mtx_file(&mat);
            let permutation = algo.run(&coo, band_len, &mat);
            let permutation_str: Vec<String> = permutation.iter().map(usize::to_string).collect();
            let output_dir = PathBuf::from("/tmp/testpermutations/").join(mat);
            if !output_dir.exists() {
                std::fs::create_dir_all(&output_dir).unwrap();
            }
            let output = output_dir.join(format!("{algo:?}.csv"));
            let mut output = File::options()
                .write(true)
                .create(true)
                .open(output)
                .unwrap();
            use std::io::Write;
            output
                .write_all(permutation_str.join(",").as_bytes())
                .unwrap();
        }
    }
}
