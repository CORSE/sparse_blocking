use crate::{mkl_bindings::*, utils::CooMatrix, F};
use operator_benchmarks::Computation;
use spmmgen::DenseMatrix;

pub struct MklDense {
    name: &'static str,
    i: i64,
    j: i64,
    k: i64,
    a: *const F,
    b: *const F,
    c: *mut F,
}

impl MklDense {
    pub fn new(
        coo: &CooMatrix,
        j: i64,
        a: &DenseMatrix<F>,
        b: &DenseMatrix<F>,
        c: &mut DenseMatrix<F>,
    ) -> Self {
        MklDense {
            name: "MKL dense",
            i: coo.shape[0] as i64,
            j,
            k: coo.shape[1] as i64,
            a: a.buf.as_ptr(),
            b: b.buf.as_ptr(),
            c: c.buf.as_mut_ptr(),
        }
    }
}
impl Computation for MklDense {
    fn name(&self) -> &str {
        self.name
    }
    fn execute(&mut self) {
        unsafe {
            cblas_sgemm(
                Layout::RowMajor,
                Transpose::No,
                Transpose::No,
                self.i,
                self.j,
                self.k,
                1.0,
                self.a,
                self.k,
                self.b,
                self.j,
                0.0,
                self.c,
                self.j,
            )
        }
    }
}
