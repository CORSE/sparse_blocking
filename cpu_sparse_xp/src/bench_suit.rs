use crate::bench_one::ArgsBenchOne;
use crate::bruteforce::ArgsBruteForce;
use crate::bruteforce::BruteForceMetaData;
use crate::bruteforce::MetaKernel;
use crate::permutation::ReorderingAlgos;
use crate::utils::dump_to_json;
use crate::utils::ImplemKind;
use clap::Args;
use glob::glob;
use log::info;
use operator_benchmarks::BenchResult;
use operator_benchmarks::BenchSummary;
use operator_benchmarks::Machine;
use spmmgen::{MicroKernel, VectorInstructions};
use std::fs::create_dir_all;
use std::process::Command;
use std::{collections::HashMap, path::PathBuf};

const CACHE_FILE: &str = "cache.json";

/// Options for the command line interface of "bench-suit" subcommand.
/// For now it try only one reordering algorithm [ReorderingAlgos::PrimesGorder].
#[derive(Args)]
pub struct ArgsBenchSuit {
    #[arg(long, default_value_t)]
    avx: VectorInstructions,

    /// Change the default output directory name (with date) to a more meaningful name.
    #[arg(long)]
    output: Option<PathBuf>,

    /// The source directory containing the mtx files.
    mtx_dir: PathBuf,

    /// All the implementation to try
    #[arg(
        value_delimiter = ' ',
        default_value = "bands permuted-bands mkl-csr mkl-bsr mkl-coo"
    )]
    implems: Vec<ImplemKind>,
}

const BRUTEFORCE_DIR: &str = "results/bruteforce";

impl ArgsBenchSuit {
    pub fn benchmark_with_cache(self, compiler: String) {
        // get cache file
        info!("Reading {CACHE_FILE}");
        let mut cached_kernels: HashMap<String, [MetaKernel; 2]> =
            std::fs::read_to_string(CACHE_FILE)
                .map(|content| serde_json::from_str(&content).unwrap())
                .unwrap_or_default();

        // create output directory of the current date
        let date_stdout = Command::new("date").arg("+%F").output().unwrap().stdout;
        let date_str = String::from_utf8(date_stdout).unwrap();
        let output_dir = PathBuf::from(format!(
            "results/{}_{}_{}",
            self.avx,
            Machine::default().name,
            date_str.trim_end()
        ));
        let output_dir = self.output.unwrap_or(output_dir);
        info!("Preparing output directory {output_dir:?}");
        if !output_dir.exists() {
            create_dir_all(&output_dir).unwrap();
        }
        let bruteforce_dir = PathBuf::from(BRUTEFORCE_DIR);
        info!("Preparing bruteforce directory {bruteforce_dir:?}");
        if !bruteforce_dir.exists() {
            create_dir_all(&bruteforce_dir).unwrap();
        }

        // compute best microkernel for files not in cache
        info!("Reading input directory {:?}", self.mtx_dir);
        let recursive_search = format!("{}/**/*.mtx", self.mtx_dir.to_str().unwrap());
        for path in glob(&recursive_search).unwrap().map(|p| p.unwrap()) {
            let path_str = path.to_str().unwrap().to_owned();
            let (new_outdir, mat) = path_str.rsplit_once('/').unwrap();
            let output_dir = output_dir.join(new_outdir);
            if !output_dir.exists() {
                create_dir_all(&output_dir).unwrap();
            }
            let output_file = format!("{mat}.json");

            let [best_np, best_p] = cached_kernels
                .get(&path_str)
                .map(|kernel| {
                    info!("Best kernels for {path_str} = {kernel:?} from cache");
                    *kernel
                })
                .unwrap_or_else(|| {
                    // lazy evaluated if not in cache
                    let result = ArgsBruteForce::new(path)
                        .with_avx(self.avx)
                        .with_permutation(ReorderingAlgos::PrimesGorder)
                        .bruteforce(compiler.clone());
                    let bruteforce_dir = bruteforce_dir.join(&output_dir);
                    if !bruteforce_dir.exists() {
                        create_dir_all(&bruteforce_dir).unwrap();
                    }
                    dump_to_json(&result, bruteforce_dir.join(&output_file));
                    let summary = result.get_summary();
                    let knp = extract_best_kernel(&summary, &result, false);
                    let kp = extract_best_kernel(&summary, &result, true);
                    let kernels = [knp, kp];

                    info!("Best kernels for {path_str} = {kernels:?}");

                    // update cache file
                    info!("Updating {CACHE_FILE}");
                    cached_kernels.insert(path_str.clone(), kernels);
                    dump_to_json(&cached_kernels, CACHE_FILE);
                    kernels
                });

            // run the benchmark vs mkl
            let result = ArgsBenchOne::new(&path_str)
                .with_kernel(MicroKernel {
                    i: best_np.i,
                    j: best_np.j,
                    avx: self.avx,
                })
                .with_kernel_permut(best_p.i, best_p.j)
                .with_reordering_algos(&[ReorderingAlgos::PrimesGorder])
                .with_implementations(&self.implems)
                .benchmark(compiler.clone());

            // write results in a json file
            dump_to_json(&result, output_dir.join(output_file));
        }
    }
}

fn extract_best_kernel(
    summary: &BenchSummary,
    result: &BenchResult<BruteForceMetaData>,
    permute: bool,
) -> MetaKernel {
    let best = summary
        .condensed
        .iter()
        .filter(|(k, _)| k.contains("permut") == permute)
        .min_by(|(_, v), (_, v2)| {
            v.execution_time
                .average
                .total_cmp(&v2.execution_time.average)
        })
        .unwrap();
    // key should look like this "I=2 J=3" for regular implementation
    // and should look like this "I=2 J=3 permuted" for permuted
    let tmp_key_str = best.0.replace("I=", "").replace("J=", "");
    let mut it = tmp_key_str.split(' ');
    let i: usize = it.next().unwrap().parse().unwrap();
    let j: usize = it.next().unwrap().parse().unwrap();
    let kernel = *result
        .metadata
        .kernels
        .iter()
        .find(|k| k.i == i && k.j == j && (k.permuted == permute))
        .unwrap();
    kernel
}
