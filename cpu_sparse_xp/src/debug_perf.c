#include <assert.h>
#include <x86intrin.h>

#define FLOAT float
#define VECSIZE 8
#define UKERNJ 4

void debug_matmul_by_bourrin(const int J, const FLOAT *const __restrict__ B,
                             FLOAT *__restrict__ C) {
  assert(J % VECSIZE == 0);
  assert(B != C);
}
