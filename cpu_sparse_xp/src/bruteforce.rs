use crate::exp::Exp;
use crate::permutation::ReorderingAlgos;
use crate::utils::*;
use crate::CONFIG;
use crate::F;

use clap::Args;
use log::info;
use operator_benchmarks::BenchResult;
use operator_benchmarks::Benchmark;
use serde::Deserialize;
use serde::Serialize;
use spmmgen::*;

use std::path::PathBuf;

/// Arguments of the command line interface for bruteforce.
/// Some options can also be modified from the code.
#[derive(Args)]
pub struct ArgsBruteForce {
    path: PathBuf,

    #[arg(required = false, default_value_t)]
    avx: VectorInstructions,

    /// Option to iterate on one i instead of a range.
    #[arg(long)]
    fix_i: Option<usize>,

    /// Option to iterate on one j instead of a range.
    #[arg(long)]
    fix_j: Option<usize>,

    /// Choose the permutation algorithm to bruteforce.
    #[arg(long)]
    permutation: Option<ReorderingAlgos>,
}

/// Information to store in json files about the kernels that we have bruteforced.
#[derive(Serialize, Deserialize, Clone, Copy, PartialEq, Eq, Debug)]
pub struct MetaKernel {
    pub i: usize,
    pub j: usize,
    pub avx: VectorInstructions,
    pub nnz_zero_filled: usize,
    pub permuted: bool,
}

/// The entiere information to save to json after bruteforcing. It will be used by bench-suit to
/// find the best kernel.
#[derive(Serialize)]
pub struct BruteForceMetaData {
    pub len_i: usize,
    pub len_k: usize,
    pub len_j: usize,
    pub nnz: usize,
    pub start_i: usize,
    pub end_i: usize,
    pub start_j: usize,
    pub end_j: usize,
    pub tile_j: Option<usize>,
    pub matrix: String,
    pub avx: VectorInstructions,
    pub kernels: Vec<MetaKernel>,
}

impl ArgsBruteForce {
    pub fn new<T>(to_path: T) -> Self
    where
        PathBuf: From<T>,
    {
        ArgsBruteForce {
            avx: VectorInstructions::default(),
            fix_i: None,
            fix_j: None,
            permutation: None,
            path: PathBuf::from(to_path),
        }
    }

    pub fn with_avx(mut self, avx: VectorInstructions) -> Self {
        self.avx = avx;
        self
    }

    pub fn with_permutation(mut self, algo: ReorderingAlgos) -> Self {
        self.permutation = Some(algo);
        self
    }

    /// A big loop on a 2d range of [MicroKernel] sizes that match the constraint of using less
    /// than the total number of vector registers available.
    /// It can also try one reordering algorithm with the right option set.
    pub fn bruteforce(self, compiler: String) -> BenchResult<BruteForceMetaData> {
        let mut bench = Benchmark::from_toml_file(CONFIG).without_progress();
        info!("Bruteforcing {:?}", self.path);
        let coo_args = CooMatrix::from_mtx_file(&self.path);
        let vecsize = self.avx.vector_size::<F>();
        let generator = CodeGenerator::new().with_compiler(compiler);
        let n_vec_regs = count_ymm_registers();
        info!("Bruteforcing with {n_vec_regs} ymm registers");
        let len_i = coo_args.shape[0];
        let len_k = coo_args.shape[1];
        let len_j: usize = 128;
        let avx = self.avx;

        // The logic here is the footprint of a kernel
        // i*j (vectors C)+ j*1 (vectors B) + i*1 (vectors A)
        // but if we interleave loads like this kernel(2,2)
        // broadcast A, load B, fma, load B, fma, broadcast A, load B, fma, load B, fma
        // the compiler can reuse 1 register for vectors from A
        // so the constraint on i and j are i*j + j + 1 <= n_vec_regs
        // assuming j >= 2 and i >= 1
        // we then have for both i and j a max value of (n_vec_regs -1) / 2

        let (start_i, end_i) = self
            .fix_i
            .map(|i| (i, i))
            .unwrap_or((1, (n_vec_regs - 1) / 2));
        let (start_j, end_j) = self
            .fix_j
            .map(|j| (j, j))
            .unwrap_or((2, (n_vec_regs - 1) / 2));

        let mut kernels: Vec<MetaKernel> =
            Vec::with_capacity((1 + end_i - start_i) * (1 + end_j - start_j));

        for i in start_i..=end_i {
            let dense_a = padded_dense_a(i, &coo_args);
            let permutation = if i > 1 {
                self.permutation
                    .map(|algo| get_perm_cache_or_algo(&self.path, &coo_args, i, algo))
            } else {
                None
            };
            for j in start_j..=end_j {
                // if we use more ymm registers than the architecture have
                // we will spill so ignore theses cases
                if i * j + j + 1 > n_vec_regs {
                    continue;
                }
                let ukern = MicroKernel { i, j, avx };
                let name = format!("I={i} J={j}");

                let pad_j = len_j.next_multiple_of(j * vecsize);
                let b: DenseMatrix<F> = DenseMatrix::filled_padding(len_k, len_j, len_k, pad_j);
                let mut c: DenseMatrix<F> =
                    DenseMatrix::zeros_padded(len_i, len_j, dense_a.pad_nrows, pad_j);
                info!(
                    "Bruteforcing i={i} (/{end_i}) j={j} (/{end_j}) J={} ",
                    c.pad_ncols,
                );

                let bands = Bands::new(&dense_a, i, None);
                kernels.push(MetaKernel {
                    i,
                    j,
                    avx,
                    nnz_zero_filled: bands.values.len(),
                    permuted: false,
                });
                let computation = Exp::new(&generator, bands, ukern, &b, &mut c).with_name(name);
                bench.run(computation);

                if let Some(perm) = &permutation {
                    let name = format!("I={i} J={j} permuted");
                    let bands = PermutedBands::new(&dense_a, i, None, perm);
                    kernels.push(MetaKernel {
                        i,
                        j,
                        avx,
                        nnz_zero_filled: bands.band.values.len(),
                        permuted: true,
                    });
                    let computation =
                        Exp::new(&generator, bands, ukern, &b, &mut c).with_name(name);
                    bench.run(computation);
                }
            }
        }

        let meta = BruteForceMetaData {
            start_i,
            end_i,
            start_j,
            end_j,
            len_j,
            len_i: coo_args.shape[0],
            len_k: coo_args.shape[1],
            nnz: coo_args.triplets.len(),
            tile_j: None,
            matrix: self.path.to_str().unwrap().to_owned(),
            avx: self.avx,
            kernels,
        };
        bench.result(meta)
    }
}
