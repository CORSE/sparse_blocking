use crate::mkl_bindings::*;
use crate::utils::CooMatrix;
use crate::F;
use operator_benchmarks::Computation;
use spmmgen::DenseMatrix;
use std::ptr::null_mut;

pub struct MklSparseImpl {
    // WE NEED THESE FIELDS BECAUSE MKL WILL READ IT BUT NOT OUR RUST CODE
    // WE AVOID READ AFTER FREE SINCE IF OWN BY FUNC THEY WILL BE DROPPED
    #[allow(dead_code)]
    rows: Vec<i64>,
    #[allow(dead_code)]
    cols: Vec<i64>,
    #[allow(dead_code)]
    nonzeros: Vec<F>,
    name: String,
    j: i64,
    k: i64,
    descr: MatrixDescr,
    a: *mut SparseMatrix,
    b: *const F,
    c: *mut F,
}
fn convert_ptr(tmp_ptr: *mut SparseMatrix, sparse_fmt: MklSparseFormats) -> *mut SparseMatrix {
    let mut res_ptr: *mut SparseMatrix = null_mut();
    use MklSparseFormats::*;
    let ret = match sparse_fmt {
        Coo => {
            res_ptr = tmp_ptr;
            SparseStatus::Sucess
        }
        Csr => unsafe { mkl_sparse_convert_csr(tmp_ptr, SparseTranspose::No, &mut res_ptr) },
        Bsr(blocsize) => unsafe {
            mkl_sparse_convert_bsr(
                tmp_ptr,
                blocsize,
                Layout::RowMajor,
                SparseTranspose::No,
                &mut res_ptr,
            )
        },
    };
    assert_eq!(ret, SparseStatus::Sucess);
    // possible memory leak here since mkl internally allocate memory
    // maybe we should destroy the temporary coo matrix used for conversion
    res_ptr
}

const DEFAULT_DESCR: MatrixDescr = MatrixDescr {
    r#type: MatrixType::General,
    mode: FillMode::Full,
    diag: DiagType::NonUnit,
};

impl MklSparseImpl {
    pub fn new(
        sparse_fmt: MklSparseFormats,
        coo_args: &CooMatrix,
        dense_b: &DenseMatrix<F>,
        dense_c: &mut DenseMatrix<F>,
    ) -> Self {
        let mut rows: Vec<i64> = coo_args.triplets.iter().map(|a| a.0 as i64).collect();
        let mut cols: Vec<i64> = coo_args.triplets.iter().map(|a| a.1 as i64).collect();
        let mut nonzeros: Vec<F> = coo_args.triplets.iter().map(|a| a.2).collect();
        let mut tmp_ptr: *mut SparseMatrix = null_mut();
        let ret = unsafe {
            mkl_sparse_s_create_coo(
                &mut tmp_ptr,
                BaseIndex::Zero,
                coo_args.shape[0] as i64,
                coo_args.shape[1] as i64,
                nonzeros.len() as i64,
                rows.as_mut_ptr(),
                cols.as_mut_ptr(),
                nonzeros.as_mut_ptr(),
            )
        };
        assert_eq!(ret, SparseStatus::Sucess);
        let a = convert_ptr(tmp_ptr, sparse_fmt);

        MklSparseImpl {
            rows,
            cols,
            nonzeros,
            name: sparse_fmt.to_string(),
            j: dense_c.pad_ncols as i64,
            k: coo_args.shape[1] as i64,
            a,
            descr: DEFAULT_DESCR,
            b: dense_b.buf.as_ptr(),
            c: dense_c.buf.as_mut_ptr(),
        }
    }
}

impl Computation for MklSparseImpl {
    fn name(&self) -> &str {
        self.name.as_str()
    }
    fn inspect(&mut self, _nexecs: u64) {
        unsafe {
            let ret = mkl_sparse_set_mm_hint(
                self.a,
                SparseTranspose::No,
                self.descr,
                Layout::RowMajor,
                self.k,
                _nexecs as i64,
            );
            assert!((ret == SparseStatus::Sucess) || (ret == SparseStatus::NotSupported));
            let ret = mkl_sparse_optimize(self.a);
            assert_eq!(ret, SparseStatus::Sucess);
        }
    }
    fn execute(&mut self) {
        unsafe {
            mkl_sparse_s_mm(
                SparseTranspose::No,
                1.0, // alpha
                self.a,
                self.descr,
                Layout::RowMajor,
                self.b,
                self.j,
                self.j,
                0.0, // beta
                self.c,
                self.j,
            );
        }
    }
}

impl Drop for MklSparseImpl {
    fn drop(&mut self) {
        unsafe {
            let ret = mkl_sparse_destroy(self.a);
            assert_eq!(ret, SparseStatus::Sucess);
        }
    }
}

#[cfg(test)]
mod test_mkl_computation {
    use super::*;
    use rstest::rstest;
    #[rstest]
    #[case(MklSparseFormats::Coo)]
    #[case(MklSparseFormats::Csr)]
    #[case(MklSparseFormats::Bsr(2))]
    fn test_sparse(#[case] sparse_format: MklSparseFormats) {
        let (m, n, k) = (102, 102, 102);
        let nnz = 10;
        let triplets: Vec<(usize, usize, F)> = (0..nnz).map(|i| (i, 4 * i + 2, 0.1)).collect();
        let mut a: Vec<F> = vec![0.0; k * m];
        for &(i, j, val) in triplets.iter() {
            a[i * n + j] = val;
        }

        let coo = CooMatrix {
            shape: [m, k],
            triplets,
        };
        let b = DenseMatrix::filled_no_padding(n, k);
        let mut c = DenseMatrix::zeros_no_padding(n, m);
        let mut computation = MklSparseImpl::new(sparse_format, &coo, &b, &mut c);
        computation.inspect(1); // 1 call
        computation.execute();

        let mut expected: Vec<F> = (0..(n * m)).map(|_| 0.0).collect();

        for i in 0..m {
            for kidx in 0..k {
                for j in 0..n {
                    expected[i * n + j] += a[i * k + kidx] * b.buf[kidx * m + j];
                }
            }
        }
        assert_eq!(c.buf, expected);
    }
}
